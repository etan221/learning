## [AWS Lambda](https://www.youtube.com/playlist?list=PLxoOrmZMsAWyBy3qwWdNhtAi-J4yLK1k9)
1. What is AWS Lambda 8:10
2. How AWS Lambda Works Internally 3:35
3. Using AWS Lambda Web Console 13:12
4. Handler function in AWS lambda 7:36
5. Event object in AWS lambda 9:54
6. Context object in AWS Lambda 9:28
7. Callback function in AWS Lambda 8:53
8. Logging inisde aws lambda 11:29
9. Creating Lambda function using zip file in AWS web console 12:56
10. Configuring AWS CLI for AWS lambda 12:52
11. Create AWS Lambda function using AWS CLI 17:56
12. Updating Lambda function code from AWS-CLI 9:21

**IAM user: etan221_dev on AWS, please login via: https://366753404928.signin.aws.amazon.com/console**



### 1. What is AWS Lambda 8:10
- FaaS (Function as a service) Architecture
- Compute Service from AWS
- Don't worry about infrastructure only focus on code.
- Auto Scaling
- Pay for what you use
- Node.js, Java, C#, Python
- Use Case:
    1. CRUD: Data -> lambda1 -> DynamoDB
    2. resize image: AWS S3 -> lambda2
    3. find random number between two numbers: API gateway -> lambda3
    
### 2. [How AWS Lambda Works Internally](https://www.youtube.com/watch?v=e8tLGC3ZadQ&list=PLxoOrmZMsAWyBy3qwWdNhtAi-J4yLK1k9&index=2)
- more requests, more containers
- less requests, less containers
- no request, no container

### 3. [Using AWS Lambda Web Console](https://www.youtube.com/watch?v=CxL9tKKW9pw&list=PLxoOrmZMsAWyBy3qwWdNhtAi-J4yLK1k9&index=3)
- arn:aws:lambda:us-east-1:366753404928:function:abc
```
exports.handler = async (event) => {
    // TODO implement
    console.log("Hello, I am learning lambda.");
    const response = {
        statusCode: 200,
        body: JSON.stringify('Hello from Lambda!'),
    };
    return response;
};
```

- arn (Amazon Resource Name)
- arn:aws:logs:us-east-1:366753404928:log-group:/aws/lambda/abc:*
### 4. [Handler function in AWS lambda](https://www.youtube.com/watch?v=46AA-c975fQ&list=PLxoOrmZMsAWyBy3qwWdNhtAi-J4yLK1k9&index=4)
- handle request: handler function runs when lambda is invoked, by S3 or API gateway
- provide the filename and the function name of the lambda function 
```
FILE_NAME.EXPORTED_FUNCTION_NAME
```
- in NodeJS, index.js
```
exports.handler = function(event, context, callback) {
}
// export a handler function to the outside world
```
```
index.handler
```
- rename function
```
exports.myhandler = ...
```
- test and get error... 
```
2020-07-29T09:03:08.118+12:00
2020-07-28T21:03:08.117Z undefined ERROR Uncaught Exception 
    {
        "errorType": "Runtime.HandlerNotFound",
        "errorMessage": "index.handler is undefined or not exported",
        "stack": [
            "Runtime.HandlerNotFound: index.handler is undefined or not exported",
            "    at Object.module.exports.load (/var/runtime/UserFunction.js:144:11)",
            "    at Object.<anonymous> (/var/runtime/index.js:43:30)",
            "    at Module._compile (internal/modules/cjs/loader.js:1133:30)",
            "    at Object.Module._extensions..js (internal/modules/cjs/loader.js:1153:10)",
            "    at Module.load (internal/modules/cjs/loader.js:977:32)",
            "    at Function.Module._load (internal/modules/cjs/loader.js:877:14)",
            "    at Function.executeUserEntryPoint [as runMain] (internal/modules/run_main.js:74:12)",
            "    at internal/main/run_main_module.js:18:47"
        ]
    }

2020-07-29T09:03:08.622+12:00
START RequestId: a0685178-858d-42b6-aa99-d84487fe1810 Version: $LATEST

2020-07-29T09:03:09.753+12:00
2020-07-28T21:03:09.752Z undefined ERROR Uncaught Exception {"errorType":"Runtime.HandlerNotFound","errorMessage":"index.handler is undefined or not exported","stack":["Runtime.HandlerNotFound: index.handler is undefined or not exported"," at Object.module.exports.load (/var/runtime/UserFunction.js:144:11)"," at Object.<anonymous> (/var/runtime/index.js:43:30)"," at Module._compile (internal/modules/cjs/loader.js:1133:30)"," at Object.Module._extensions..js (internal/modules/cjs/loader.js:1153:10)"," at Module.load (internal/modules/cjs/loader.js:977:32)"," at Function.Module._load (internal/modules/cjs/loader.js:877:14)"," at Function.executeUserEntryPoint [as runMain] (internal/modules/run_main.js:74:12)"," at internal/main/run_main_module.js:18:47"]}

2020-07-29T09:03:10.104+12:00
END RequestId: a0685178-858d-42b6-aa99-d84487fe1810

2020-07-29T09:03:10.104+12:00
REPORT RequestId: a0685178-858d-42b6-aa99-d84487fe1810 Duration: 1481.68 ms Billed Duration: 1500 ms Memory Size: 128 MB Max Memory Used: 14 MB

2020-07-29T09:03:10.104+12:00
Unknown application error occurred Runtime.HandlerNotFound

2020-07-29T09:03:10.186+12:00
2020-07-28T21:03:10.186Z undefined ERROR Uncaught Exception {"errorType":"Runtime.HandlerNotFound","errorMessage":"index.handler is undefined or not exported","stack":["Runtime.HandlerNotFound: index.handler is undefined or not exported"," at Object.module.exports.load (/var/runtime/UserFunction.js:144:11)"," at Object.<anonymous> (/var/runtime/index.js:43:30)"," at Module._compile (internal/modules/cjs/loader.js:1133:30)"," at Object.Module._extensions..js (internal/modules/cjs/loader.js:1153:10)"," at Module.load (internal/modules/cjs/loader.js:977:32)"," at Function.Module._load (internal/modules/cjs/loader.js:877:14)"," at Function.executeUserEntryPoint [as runMain] (internal/modules/run_main.js:74:12)"," at internal/main/run_main_module.js:18:47"]}

```
- goto Lambda > Functions > abc > Edit basic settings
- rename Handler: index.myhandler
- save, and then test
```
2020-07-29T09:14:49.645+12:00
START RequestId: 27c08b20-22aa-4e5f-8860-beee325b63ae Version: $LATEST

2020-07-29T09:14:49.687+12:00
2020-07-28T21:14:49.668Z	27c08b20-22aa-4e5f-8860-beee325b63ae	INFO	Hello, I am learning lambda.

2020-07-29T09:14:49.689+12:00
END RequestId: 27c08b20-22aa-4e5f-8860-beee325b63ae

2020-07-29T09:14:49.689+12:00
REPORT RequestId: 27c08b20-22aa-4e5f-8860-beee325b63ae	Duration: 43.52 ms	Billed Duration: 100 ms	Memory Size: 128 MB	Max Memory Used: 64 MB	Init Duration: 123.48 ms
```

### 5. [Event object in AWS lambda](https://www.youtube.com/watch?v=MUrcjIqpz6o&list=PLxoOrmZMsAWyBy3qwWdNhtAi-J4yLK1k9&index=5)
```
GET https://domain:port/user/eric?location=nz&age=18
```
```
exports.handler = function(event, context, callback) {
}
// event: input paramete, could be from S3, API gateway, DynamoDB
```
```
{
    "path":"/user/eric",
    "header": {
        "Accept":"application/json",
    },
    "pathParameters": { "userName":"eric" },
    "requestContext": {
        "requestId": "41b45ea3-70b5-11e6-b7bd-69b5aaebc7d9",
    },
    "httpMethod": "GET",
    "queryStringParameters": { "location":"nz", "age": "18" }
}
```
```
2020-07-29T10:05:30.452+12:00
START RequestId: d9f0db3f-7eaf-421c-abe2-c33379bdee2d Version: $LATEST

2020-07-29T10:05:30.454+12:00
2020-07-28T22:05:30.454Z d9f0db3f-7eaf-421c-abe2-c33379bdee2d INFO Hello, I am learning lambda.

2020-07-29T10:05:30.454+12:00
2020-07-28T22:05:30.454Z d9f0db3f-7eaf-421c-abe2-c33379bdee2d INFO Event:

2020-07-29T10:05:30.459+12:00
2020-07-28T22:05:30.459Z d9f0db3f-7eaf-421c-abe2-c33379bdee2d INFO { key1: 'value1', key2: 'value2', key3: 'value3' }

2020-07-29T10:05:30.519+12:00
END RequestId: d9f0db3f-7eaf-421c-abe2-c33379bdee2d

2020-07-29T10:05:30.519+12:00
REPORT RequestId: d9f0db3f-7eaf-421c-abe2-c33379bdee2d Duration: 67.11 ms Billed Duration: 100 ms Memory Size: 128 MB Max Memory Used: 64 MB Init Duration: 131.08 ms

```
### 6. [Context object in AWS Lambda](https://www.youtube.com/watch?v=BDLWWW_H6BM&list=PLxoOrmZMsAWyBy3qwWdNhtAi-J4yLK1k9&index=6)
- provide runtime information of the lambda function - functionName
- provide details about the execution environment - memory limit
- provide some properties and methods

Properties  

context.functionName
context.functionVersion
context.invokedFunctionARN
context.memoryLimitMB
context.awsRequestId
context.logGroupName
context.logStreamName

Methods
context.getRemainingTimeInMillis()
timeout=10s for function lambda1

```
exports.handler = async (event, context, callback) => {
    ...
    console.log("context:");
    console.log(context);
    ...
};
```
```
2020-07-29T10:15:54.318+12:00
START RequestId: 6b55383b-a42d-4202-8c4c-3770aa91e28b Version: $LATEST

2020-07-29T10:15:54.321+12:00
2020-07-28T22:15:54.320Z 6b55383b-a42d-4202-8c4c-3770aa91e28b INFO Hello, I am learning lambda.

2020-07-29T10:15:54.321+12:00
2020-07-28T22:15:54.320Z	6b55383b-a42d-4202-8c4c-3770aa91e28b	INFO	context:

2020-07-29T10:15:54.415+12:00
2020-07-28T22:15:54.395Z	6b55383b-a42d-4202-8c4c-3770aa91e28b	INFO	{
  callbackWaitsForEmptyEventLoop: [Getter/Setter],
  succeed: [Function],
  fail: [Function],
  done: [Function],
  functionVersion: '$LATEST',
  functionName: 'abc',
  memoryLimitInMB: '128',
  logGroupName: '/aws/lambda/abc',
  logStreamName: '2020/07/28/[$LATEST]5f973eb770eb4a89b63e1cdfb82eb426',
  clientContext: undefined,
  identity: undefined,
  invokedFunctionArn: 'arn:aws:lambda:ap-southeast-2:366753404928:function:abc',
  awsRequestId: '6b55383b-a42d-4202-8c4c-3770aa91e28b',
  getRemainingTimeInMillis: [Function: getRemainingTimeInMillis]
}

2020-07-29T10:15:54.416+12:00
END RequestId: 6b55383b-a42d-4202-8c4c-3770aa91e28b

2020-07-29T10:15:54.416+12:00
REPORT RequestId: 6b55383b-a42d-4202-8c4c-3770aa91e28b	Duration: 97.85 ms	Billed Duration: 100 ms	Memory Size: 128 MB	Max Memory Used: 64 MB	Init Duration: 124.41 ms
```
> context.getRemainingTimeInMillis()
```
exports.handler = async (event, context, callback) => {
    ...
    console.log("context.getRemainingTimeInMillis():");
    console.log(context.getRemainingTimeInMillis());
    ...
};
```
```
2020-07-29T10:20:36.652+12:00
2020-07-28T22:20:36.652Z 4368e452-62ca-4e9f-af76-39dbe86c781f INFO context.getRemainingTimeInMillis():

2020-07-29T10:20:36.652+12:00
2020-07-28T22:20:36.652Z	4368e452-62ca-4e9f-af76-39dbe86c781f	INFO	2997

```

### 7. [Callback function in AWS Lambda](https://www.youtube.com/watch?v=kNl1LEs_QXU&list=PLxoOrmZMsAWyBy3qwWdNhtAi-J4yLK1k9&index=7)
- exit point of a lambda function
- callback parameter in handler function is optional
- if no callback is present then it means callback is called without any parameter
```
callback (param1, param2)
```
- if success, callback(null, Object result)
- if error, callback(error)

#### when sucess
- callback(null, object); 
- indicates success with information returned to the caller
```
exports.handler = async (event, context, callback) => {
    const response = {
        statusCode: 200,
        body: JSON.stringify('Hello from Lambda!'),
    };
    callback(null, response);
};
```
```
Response:
{
  "statusCode": 200,
  "body": "\"Hello from Lambda!\""
}

Request ID:
"b596a5ef-90ba-4a47-abbb-a1731ba10108"

Function logs:
START RequestId: b596a5ef-90ba-4a47-abbb-a1731ba10108 Version: $LATEST
END RequestId: b596a5ef-90ba-4a47-abbb-a1731ba10108
REPORT RequestId: b596a5ef-90ba-4a47-abbb-a1731ba10108	Duration: 18.39 ms	Billed Duration: 100 ms	Memory Size: 128 MB	Max Memory Used: 63 MB	Init Duration: 122.90 ms	
```
- indicates success but no information returned to the caller
- no callback(), have callback(), or callback(null); 
```
exports.handler = async (event, context, callback) => {
};
```
or 
```
exports.handler = async (event, context, callback) => {
    callback();
};
```
or
```
exports.handler = async (event, context, callback) => {
    callback(null);
};
```
```
Response:
null

Request ID:
"4b147b7b-41e8-48c0-88b9-47c417f59de8"

Function logs:
START RequestId: 4b147b7b-41e8-48c0-88b9-47c417f59de8 Version: $LATEST
END RequestId: 4b147b7b-41e8-48c0-88b9-47c417f59de8
REPORT RequestId: 4b147b7b-41e8-48c0-88b9-47c417f59de8	Duration: 58.65 ms	Billed Duration: 100 ms	Memory Size: 128 MB	Max Memory Used: 64 MB	Init Duration: 144.57 ms	
```
#### when fail
- callback(error); 
- indicates error with error information returned to the caller
```
exports.handler = async (event, context, callback) => {
    callback("This is an error");
};
```
```
Response:
{
  "errorType": "string",
  "errorMessage": "This is an error",
  "trace": []
}

Request ID:
"f5408c7b-2cd6-4082-90c3-d582632401a6"

Function logs:
START RequestId: f5408c7b-2cd6-4082-90c3-d582632401a6 Version: $LATEST
2020-07-28T23:11:01.990Z	f5408c7b-2cd6-4082-90c3-d582632401a6	ERROR	Invoke Error 	{"errorType":"Error","errorMessage":"This is an error","stack":["Error: This is an error","    at _homogeneousError (/var/runtime/CallbackContext.js:12:12)","    at postError (/var/runtime/CallbackContext.js:29:54)","    at callback (/var/runtime/CallbackContext.js:41:7)","    at /var/runtime/CallbackContext.js:104:16","    at Runtime.exports.handler (/var/task/index.js:2:5)","    at Runtime.handleOnce (/var/runtime/Runtime.js:66:25)"]}
END RequestId: f5408c7b-2cd6-4082-90c3-d582632401a6
REPORT RequestId: f5408c7b-2cd6-4082-90c3-d582632401a6	Duration: 70.01 ms	Billed Duration: 100 ms	Memory Size: 128 MB	Max Memory Used: 64 MB	Init Duration: 122.17 ms	
```
### 8. [Logging inisde aws lambda](https://www.youtube.com/watch?v=5oCEoAvhX8M&list=PLxoOrmZMsAWyBy3qwWdNhtAi-J4yLK1k9&index=8)
- use AWS CloudWatch to write logs
- one Log Group Name created: context.logGroupName
- multiple Log Stream Name created: context.logStreamName
- Lambda Nodejs logs can be written using
    - console.log() 
    - console.info() 
    - console.warn() 
    - console.error() 
    
```
exports.handler = async (event, context, callback) => {
    // TODO implement
    console.log("Hello, I am learning lambda.");
    
    const response = {
        statusCode: 200,
        body: JSON.stringify('Hello from Lambda!'),
    };
    console.log("logging response:", response);
    console.info("info log:", response);
    console.warn("warn log:", response);
    console.error("error log:", response);
    
    return response;
};
```
execution result:
```
Response:
{
  "statusCode": 200,
  "body": "\"Hello from Lambda!\""
}

Request ID:
"e2cdb8e4-5390-4d79-910f-9aef23861f30"

Function logs:
START RequestId: e2cdb8e4-5390-4d79-910f-9aef23861f30 Version: $LATEST
2020-07-28T23:01:14.744Z	e2cdb8e4-5390-4d79-910f-9aef23861f30	INFO	Hello, I am learning lambda.
2020-07-28T23:01:14.763Z	e2cdb8e4-5390-4d79-910f-9aef23861f30	INFO	logging response: { statusCode: 200, body: '"Hello from Lambda!"' }
2020-07-28T23:01:14.781Z	e2cdb8e4-5390-4d79-910f-9aef23861f30	INFO	info log: { statusCode: 200, body: '"Hello from Lambda!"' }
2020-07-28T23:01:14.801Z	e2cdb8e4-5390-4d79-910f-9aef23861f30	WARN	warn log: { statusCode: 200, body: '"Hello from Lambda!"' }
2020-07-28T23:01:14.841Z	e2cdb8e4-5390-4d79-910f-9aef23861f30	ERROR	error log: { statusCode: 200, body: '"Hello from Lambda!"' }
END RequestId: e2cdb8e4-5390-4d79-910f-9aef23861f30
REPORT RequestId: e2cdb8e4-5390-4d79-910f-9aef23861f30	Duration: 100.17 ms	Billed Duration: 200 ms	Memory Size: 128 MB	Max Memory Used: 64 MB	Init Duration: 130.65 ms	
```

### 9. [Creating Lambda function using zip file in AWS web console](https://www.youtube.com/watch?v=3Ik7hIeC1Pk&list=PLxoOrmZMsAWyBy3qwWdNhtAi-J4yLK1k9&index=9)
- using the online code editor
- uploading a zip file
- app.zip
- handler: [folder]/[filename].[function_name]
- handler: app/index.handler, 

### 10. [Configuring AWS CLI for AWS lambda](https://www.youtube.com/watch?v=abv_1PiM40w&list=PLxoOrmZMsAWyBy3qwWdNhtAi-J4yLK1k9&index=10)
- web console: fine for learning or small lambdba function, tedious when developing production 
- awscli: write script locally, then deploy when finish
1. Installing AWS CLI
2. Creating user in AWS IAM
3. Configure AWS CLI
Prerequisites:
- Python
- Pip (package manager of Python)
- sudo pip install awscli