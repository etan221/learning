## A. Setup environment for Angular

### 1. [Install Chocolately](https://chocolatey.org/install)
cmd, Run Powershell as administrator
 
`Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))`

`choco /?`
```
Chocolatey v0.10.15
```
### 2. Install development tools 
`choco install -y git nodejs visualstudiocode`
```
Chocolatey installed 9/9 packages.
 See the log for details (C:\ProgramData\chocolatey\logs\chocolatey.log).

Installed:
 - visualstudiocode v1.23.1.20180730
 - chocolatey-core.extension v1.3.5.1
 - nodejs v14.6.0
 - vscode.install v1.47.2
 - git.install v2.27.0
 - vscode v1.47.2
 - nodejs.install v14.6.0
 - dotnet4.5.2 v4.5.2.20140902
 - git v2.27.0
>
```
### 3. Verify version
new cmd, run as administrator  

`npm -v`
```
6.14.6
```
`node -v`
```
v14.6.0
```

`git version`
```
git version 2.27.0.windows.1
```
`code -v`
```
1.47.2
17299e413d5590b14ab0340ea477cdd86ff13daf
x64
```

### 4. Install Angualr CLI

`npm i -g @angular/cli`
```
added 280 packages from 206 contributors in 49.655s
```
`ng --version`
```
Angular CLI: 10.0.4
Node: 14.6.0
OS: win32 x64

Angular: 2.4.10
... common, compiler, compiler-cli, core, forms, http
... platform-browser, platform-browser-dynamic
Ivy Workspace: <error>

Package                      Version
------------------------------------------------------
@angular-devkit/architect    0.1000.4 (cli-only)
@angular-devkit/core         10.0.4 (cli-only)
@angular-devkit/schematics   10.0.4 (cli-only)
@angular/router              3.4.10
@angular/tsc-wrapped         0.5.2
@ngtools/json-schema         1.1.0
@ngtools/webpack             1.10.2
@schematics/angular          10.0.4 (cli-only)
@schematics/update           0.1000.4 (cli-only)
rxjs                         5.5.12
typescript                   2.0.10
webpack                      2.2.0
```


### 5. install VSCode extension: Angular Extension Pack, by Will

## B. up and run angular webapp
### 1. Create new project  
`cd C:\opt\uoa_project`

`ng new demo1`
```
? Would you like to add Angular routing? Yes
? Which stylesheet format would you like to use? CSS
```
### 2. Run demo1 project
`cd demo1`

`npm start`

```
> demo1@0.0.0 start C:\opt\uoa_project\demo1
> ng serve

Date: 2020-07-27T02:30:37.389Z - Hash: 1bf258de9e4aad4708a6 - Time: 12304ms
** Angular Live Development Server is listening on localhost:4200, open your browser on http://localhost:4200/ **
: Compiled successfully.
```

## C. deploy angular to s3
- https://youtu.be/ZkYsMBNR_eY?t=297

`cd C:\opt\uoa_project\demo1`

`ng new demo1`

`ng serve` or `npm start`

`ng build`

- upload `dist` folder to the newly created S3 bucket

- set the S3 bucket as static website hosting

- set permission
```
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "AllowPublicReadAccess",
      "Effect": "Allow",
      "Principal": "*",
      "Action": [
        "s3:GetObject"
      ],
      "Resource": [
        "arn:aws:s3:::uoa-research-sandbox/*"
      ]
    }
  ]
}
```
- or
```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "AllowPublicReadAccess",
            "Effect": "Allow",
            "Principal": "*",
            "Action": "s3:GetObject",
            "Resource": "arn:aws:s3:::uoa-research-sandbox/*"
        },
        {
            "Sid": "2",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::cloudfront:user/CloudFront Origin Access Identity E29LF84TSH6R8O"
            },
            "Action": "s3:GetObject",
            "Resource": "arn:aws:s3:::uoa-research-sandbox/*"
        }
    ]
}
```
- or
```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "2",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::cloudfront:user/CloudFront Origin Access Identity E29LF84TSH6R8O"
            },
            "Action": "s3:GetObject",
            "Resource": "arn:aws:s3:::uoa-budget-sandbox/*"
        },
        {
            "Sid": "BlockNonSSL",
            "Effect": "Deny",
            "Principal": "*",
            "Action": "s3:*",
            "Resource": [
                "arn:aws:s3:::uoa-budget-sandbox",
                "arn:aws:s3:::uoa-budget-sandbox/*"
            ],
            "Condition": {
                "Bool": {
                    "aws:SecureTransport": "false"
                }
            }
        },
        {
            "Sid": "3",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::cloudfront:user/CloudFront Origin Access Identity E1ZA860FKNA8R"
            },
            "Action": "s3:GetObject",
            "Resource": "arn:aws:s3:::uoa-budget-sandbox/*"
        }
    ]
}
```
- uoa wayfinding
```html
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "BlockNonSSL",
            "Effect": "Deny",
            "Principal": "*",
            "Action": [
                "s3:PutObject",
                "s3:GetObject"
            ],
            "Resource": "arn:aws:s3:::uoa-wayfinding-sandbox/*",
            "Condition": {
                "Bool": {
                    "aws:SecureTransport": "false"
                }
            }
        },
        {
            "Sid": "2",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::cloudfront:user/CloudFront Origin Access Identity ESWSXZSES9LSL"
            },
            "Action": "s3:GetObject",
            "Resource": "arn:aws:s3:::uoa-wayfinding-sandbox/*"
        }
    ]
}
```
- aws
```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "AllowPublicReadAccess",
            "Effect": "Allow",
            "Principal": "*",
            "Action": "s3:GetObject",
            "Resource": "arn:aws:s3:::research-budget-sandbox/*"
        },
        {
            "Sid": "BlockNonSSL",
            "Effect": "Deny",
            "Principal": "*",
            "Action": "s3:*",
            "Resource": [
                "arn:aws:s3:::research-budget-sandbox",
                "arn:aws:s3:::research-budget-sandbox/*"
            ],
            "Condition": {
                "Bool": {
                    "aws:SecureTransport": "false"
                }
            }
        }
    ]
}```
- demo1\package.json, add `"aws-deploy": "aws s3 sync dist/demo1/ s3://angular-demo1"`  

`cd C:\opt\uoa_project\demo1`

`ng build && npm run aws-deploy`

URL for public access 
- http://angular-demo1.s3-website-ap-southeast-2.amazonaws.com

## D. deploy angular to UOA s3

- [Resolve AWS S3 Access Denied](https://youtu.be/DT_5m8oufpQ?t=271)

(https://stackoverflow.com/questions/56094367/s3-bucket-aws-you-cant-grant-public-access-because-block-public-access-settings)

- `npm run build`
- `npm run aws-deploy`
## E. deploy ionic/angular to s3

## Reference
   - frontend app with uoa style
       - app: [contact-centre](https://bitbucket.org/uoa/contact-center-frontend/src/master)
   - starter kit with ionic/angular
       - app: [ionic shell](https://bitbucket.org/uoa/ionic-shell-application/src/master/)
   - same technology but simple app
       - FE: Angular + BE: API Gateway + Lambda + DynamoDB
       - [solution design](https://wiki.auckland.ac.nz/display/UoASA/Wayfinding) for the [Wayfinding app](https://bitbucket.org/uoa/uoa-wayfinding-backend/src/master)
   - [test environment](https://profile-page.test.auckland.ac.nz/student/1526469)
   
   
## E. Angular

C:\Users\etan221>cd C:\opt\uoa_project\demo1

C:\opt\uoa_project\demo1>ng generate component page1
CREATE src/app/page1/page1.component.html (20 bytes)
CREATE src/app/page1/page1.component.spec.ts (621 bytes)
CREATE src/app/page1/page1.component.ts (271 bytes)
CREATE src/app/page1/page1.component.css (0 bytes)
UPDATE src/app/app.module.ts (471 bytes)


`cd C:\opt\uoa_project\ex1`

`npm start`

```
> demo1@0.0.0 start C:\opt\uoa_project\ex1
> ng serve

The serve command requires to be run in an Angular project, but a project definition could not be found.
npm ERR! code ELIFECYCLE
npm ERR! errno 1
npm ERR! demo1@0.0.0 start: `ng serve`
npm ERR! Exit status 1
npm ERR!
npm ERR! Failed at the demo1@0.0.0 start script.
npm ERR! This is probably not a problem with npm. There is likely additional logging output above.
npm WARN Local package.json exists, but node_modules missing, did you mean to install?

npm ERR! A complete log of this run can be found in:
npm ERR!     C:\Users\etan221\AppData\Roaming\npm-cache\_logs\2020-08-04T23_24_39_655Z-debug.log
```

`ng update`

```
The "@angular-devkit/schematics" package cannot be resolved from the workspace root directory. This may be due to an unsupported node modules structure.
Please remove both the "node_modules" directory and the package lock file; and then reinstall.
If this does not correct the problem, please temporarily install the "@angular-devkit/schematics" package within the workspace. It can be removed once the update is complete.
```

`npm ls @angular-devkit/schematics`

`npm install @angular-devkit/schematics`

`ng update @angular/cli`

`ng update @angular/core`



https://www.w3schools.com/jsref/event_onkeypress.asp

https://www.w3schools.com/jsref/jsref_length_string.asp

### Workaround to fix something
- `ng build`

```
WARNING in ./node_modules/crypto-js/core.js
  Module not found: Error: Can't resolve 'crypto' in 'C:\opt\uoa_project\research-budget\node_modules\crypto-js'
```

[fixed: change property](https://gist.github.com/niespodd/1fa82da6f8c901d1c33d2fcbb762947d)
- `node_modules/angular-devkit/build-angular/src/angular-cli-files/models/webpack-configs/browser.js`
- `node: {crypto: true, stream: true}`
- result: warning removed and but crashed: authorization
    -   Nicolas: ah yeah if you removed crypto js the PKCE will not work anymore
    
### fixed: 

dist/research-budget


If you're meaning the warning messages, I just had to add this to my 
- package.json
```
  "browser": {
    "crypto": false
  }
```
- angular.json
```
      "architect": {
        "build": {
          "builder": "@angular-devkit/build-angular:browser",
          "options": {
            "aot": true,
            "outputPath": "www",
            "index": "src/index.html",
            "main": "src/main.ts",
            "tsConfig": "tsconfig.base.json",
            "polyfills": "src/polyfills.ts",
            "assets": [
              "./src/assets"
            ],
            "styles": [
              "src/styles.scss"
            ],
            "scripts": [],
            "allowedCommonJsDependencies": [
              "graphql-tag",
              "crypto-js", <-----------------
              "zen-observable"
            ]
          },
```

###