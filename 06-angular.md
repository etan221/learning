- [Angular](#angular)
  * [A. Data Binding](#a-data-binding)
  * [B. Template reference variable](#b-template-reference-variable)
  * [C. Directives](#c-directives)
    + [1. component directives](#1-component-directives)
    + [2. attribute directives](#2-attribute-directives)
      - [a. ngStyle](#a-ngstyle)
        * [i.  basic](#i--basic)
        * [ii. via method](#ii-via-method)
        * [iii. simplified, multiple](#iii-simplified--multiple)
      - [b. ngClass](#b-ngclass)
        * [i.  basic](#i--basic-1)
        * [ii. simplified](#ii-simplified)
    + [3. structural directives](#3-structural-directives)
      - [i. ngIf](#i-ngif)
      - [ii. ngSwitch](#ii-ngswitch)
      - [iii. ngFor](#iii-ngfor)
  * [D. Pipes](#d-pipes)
    + [1. uppercase / lowercase](#1-uppercase---lowercase)
    + [2. number](#2--number--https---angulario-api-common-decimalpipe-)
    + [3. currency](#3--currency--https---angulario-api-common-currencypipe-)
    + [4. percent](#4--percent--https---angulario-api-common-percentpipe-)
    + [5. date](#5-date)
    + [6. json](#6--json--https---angulario-api-common-jsonpipe-)
    + [7. slice](#7--slice--https---angulario-api-common-slicepipe-)
  * [E. safe navigation operator](#e--safe-navigation-operator--https---angulario-guide-template-expression-operators-)
    + [avoid error type of variable in TypeScript](#avoid-error-type-of-variable-in-typescript)

<small><i><a href='http://ecotrust-canada.github.io/markdown-toc/'>Table of contents generated with markdown-toc</a></i></small>


# Angular

Reference:
- [API list](https://angular.io/api)
- [Angular Essentials](https://connected.percipio.com/books/3c533365-36cb-44c2-8a2e-ee69ed983c98), on Percipio 

## A. Data Binding
1. interpolation
`{{ property }}`
1. property binding
`[property] = 'statement'`
1. event binding
`(event)='someMethod($event)'`
1. two-way binding
`[(ngModel)]='property'`

## B. Template reference variable
`#name`
- create a local variable in the html template
- scope:  only in this component
- store DOM object of the tag 
- usage: allow `.ts` to access attributes of the DOM object by `(event)='someMethod($event)'`
- `#name`, a kind of syntax sugar, is the same as `ref-name`  

## C. Directives
### 1. component directives 
`ng g c footer` 

```
@Component({
    selector: 'app-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.css'],
    encapsulation: ViewEncapsulation.None
})
``` 
`<app-footer></app-footer>`

### 2. attribute directives

#### a. ngStyle 
##### i.  basic
- `[ngStyle]="{'font-size': (12+counter) + 'px'}"`
- gen by code snipplet: `a-ngStyle`
`[ngStyle]="{style: expression}"`

##### ii. via method 
`[ngStyle]="getStyle()"`

```js
getStyle() {
    return {'font-size': (12+ this.counter) + 'px' }
}
``` 
##### iii. simplified, multiple
`[style.font-size]="(12+counter)+'px'"`

`[style.color]="'red'"`

#### b. ngClass 
- add css class if expression returns true

##### i.  basic
- `[ngClass]="{'highlight': counter % 2 == 0 }"`

- component.css
```css
.highlight {
    background-color: yellow;
}
```

- gen by code snipplet: `a-ngClass`

`[ngClass]="{cssClass: expression}"`

##### ii. simplified
`[class.highlight]="counter % 2 == 0"`

### 3. structural directives
- change DOM structure by adding or removing DOM object
- `*ngIf`, `*ngFor`, `*ngSwitchDefault`, `*ngSwitchCase`, but don't add `*` before `ngSwitch`

#### i. ngIf
- tag is added if expression return true
- tag is removed if expression return false
- `*ngIf="counter % 2 == 0"`

- gen by code snipplet: `a-ngIf`
`*ngIf="expression"`

**beware of the impact to directives which are included in the tag, e.g. data values will be reset/lost.**
    
#### ii. ngSwitch
- use `ng-container`, instead of `div`, to avoid additional useless `<div>` which affect DOM structure 
```html
<ng-container [ngSwitch]="counter % 2 ">
    <ng-container *ngSwitchCase="0">
        ...
    </ng-container>
    <ng-container *ngSwitchCase="1">
       ...
    </ng-container>
    <ng-container *ngSwitchDefault>
       ...
    </ng-container>
</ng-container>
```
                  
- gen by code snipplet: `a-ngSwitch`
```html
<div [ngSwitch]="conditionExpression">
  <div *ngSwitchCase="expression">output</div>
  <div *ngSwitchDefault>output2</div>
</div>
```

#### iii. ngFor
- use frequently
```html
<article class="post" id="post{{idx}}" *ngFor="let article of articles; let idx=index">
  <header class="post-header">
    <h2 class="post-title">
      <a [href]="article.href">{{ article.title }}</a>
    </h2>
    <div class="post-info clearfix">
      <span class="post-date"><i class="glyphicon glyphicon-calendar"></i>{{ article.date }}</span>
      <span class="post-author"><i class="glyphicon glyphicon-user"></i>
        <a href="http://blog.miniasp.com/author/will.aspx">{{ article.author }}</a></span>
      <span class="post-category"><i class="glyphicon glyphicon-folder-close"></i>
        <a [href]="article['category-link']">{{ article.category }}</a></span>
    </div>
  </header>
  <section class="post-body text" [innerHtml]="article.summary">
  </section>
</article>
```
- gen by code snipplet: `a-ngFor`
- `*ngFor="let item of list"`

- disable tslint, vscode: `Ctr + ,` open settings, `tslint enable` (not working, need to investigate)
- use `article['category-link']` to handle `-` , same meaning as `article.category-link`
- `[innerHTML]`, angular sanitizing HTML stripped some content, see http://g.co/ng/security#xss`
   - https://angular.io/guide/security#xss
   - https://angular.tw/guide/security#xss

## D. Pipes

- goto www.angular.io
- search: [Pipe Guide](https://angular.io/guide/pipes), [API](https://angular.io/api/core/Pipe), [common pipes](https://angular.io/api/common#pipes)
- Pipes are simple functions you can use in template expressions to accept an input value and return a transformed value
- The pipe operator '|' pass the template output of a component to a pipe component, then show the result of the pipe component onto screen. 

```js
import { Component } from '@angular/core';

@Component({
    selector: 'app-hero-birthday',
    template: `<p>The hero's birthday is {{ birthday | date }}</p>`
})
export class HeroBirthdayComponent {
    birthday = new Date(1988, 3, 15); // April 15, 1988
}
```
### 1. uppercase / lowercase
- change string value to uppercase/lowercase 
- `<a [href]="article.href">{{ article.title | uppercase }}</a>`
- `<a [href]="article.href">{{ article.title | lowercase }}</a>`
- may use self-defined pipe
- best for formatting
### 2. [number](https://angular.io/api/common/DecimalPipe)
- `{{ value_expression | number [ : digitsInfo [ : locale ] ] }}`
- 
```js
@Component({
  selector: 'number-pipe',
  template: `<div>
    <!--output '2.718'-->
    <p>e (no formatting): {{e | number}}</p>

    <!--output '002.71828'-->
    <p>e (3.1-5): {{e | number:'3.1-5'}}</p>

    <!--output '0,002.71828'-->
    <p>e (4.5-5): {{e | number:'4.5-5'}}</p>

    <!--output '0 002,71828'-->
    <p>e (french): {{e | number:'4.5-5':'fr'}}</p>

    <!--output '3.14'-->
    <p>pi (no formatting): {{pi | number}}</p>

    <!--output '003.14'-->
    <p>pi (3.1-5): {{pi | number:'3.1-5'}}</p>

    <!--output '003.14000'-->
    <p>pi (3.5-5): {{pi | number:'3.5-5'}}</p>

    <!--output '-3' / unlike '-2' by Math.round()-->
    <p>-2.5 (1.0-0): {{-2.5 | number:'1.0-0'}}</p>
  </div>`
})
export class NumberPipeComponent {
  pi: number = 3.14;
  e: number = 2.718281828459045;
}
```
### 3. [currency](https://angular.io/api/common/CurrencyPipe)
- `{{ value_expression | currency [ : currencyCode [ : display [ : digitsInfo [ : locale ] ] ] ] }}`
-
```
@Component({
  selector: 'currency-pipe',
  template: `<div>
    <!--output '$0.26'-->
    <p>A: {{a | currency}}</p>

    <!--output 'CA$0.26'-->
    <p>A: {{a | currency:'CAD'}}</p>

    <!--output 'CAD0.26'-->
    <p>A: {{a | currency:'CAD':'code'}}</p>

    <!--output 'CA$0,001.35'-->
    <p>B: {{b | currency:'CAD':'symbol':'4.2-2'}}</p>

    <!--output '$0,001.35'-->
    <p>B: {{b | currency:'CAD':'symbol-narrow':'4.2-2'}}</p>

    <!--output '0 001,35 CA$'-->
    <p>B: {{b | currency:'CAD':'symbol':'4.2-2':'fr'}}</p>

    <!--output 'CLP1' because CLP has no cents-->
    <p>B: {{b | currency:'CLP'}}</p>
  </div>`
})
export class CurrencyPipeComponent {
  a: number = 0.259;
  b: number = 1.3495;
}
```
### 4. [percent](https://angular.io/api/common/PercentPipe)
- `{{ value_expression | percent [ : digitsInfo [ : locale ] ] }}`
-
```
@Component({
  selector: 'percent-pipe',
  template: `<div>
    <!--output '26%'-->
    <p>A: {{a | percent}}</p>

    <!--output '0,134.950%'-->
    <p>B: {{b | percent:'4.3-5'}}</p>

    <!--output '0 134,950 %'-->
    <p>B: {{b | percent:'4.3-5':'fr'}}</p>
  </div>`
})
export class PercentPipeComponent {
  a: number = 0.259;
  b: number = 1.3495;
}
```
### 5. date
- `{{ article.date | date:"yyyy-MM-dd" }} // output: 2016-04-30`
-
```
<div>
  <p>today | date: Aug 12, 2020: {{today | date}}</p>
  <p>today | date:'fullDate': Wednesday, August 12, 2020: {{today | date:'fullDate'}}</p>
  <p>today | date:'h:mm a z' 3:58 PM GMT+12 {{today | date:'h:mm a z'}}</p>
</div>
```
### 6. [json](https://angular.io/api/common/JsonPipe)
- converts a value into its JSON-format representation
- useful for debugging
- `<pre>{{ article }}</pre> // output: [object Object]`
- `<pre>{{ article | json }}</pre> // output: `
```
{
  "id": 1,
  "href": "http://blog.miniasp.com/post/2016/04/30/Visual-Studio-Code-from-Command-Prompt-notes.aspx",
  "title": "從命令提示字元中開啟 Visual Studio Code 如何避免顯示惱人的偵錯訊息",
  "date": "2016/04/30 18:05",
  "author": "Will 保哥",
  "category": "Visual Studio",
  "category-link": "http://blog.miniasp.com/category/Visual-Studio.aspx",
  "summary": "<p>由於我的 Visual Studio Code 大部分時候都是在命令提示字元下啟動，所以只要用 <strong><font color='#ff0000' face='Consolas'>code .</font></strong>就可以快速啟動 Visual Studio Code 並自動開啟目前所在資料夾。不過不知道從哪個版本開始，我在啟動 Visual Studio Code 之後，卻開始在原本所在的命令提示字元視窗中出現一堆惱人的偵錯訊息，本篇文章試圖解析這個現象，並提出解決辦法。</p><p>... <a class='more' href='http://blog.miniasp.com/post/2016/04/30/Visual-Studio-Code-from-Command-Prompt-notes.aspx#continue'>繼續閱讀</a>...</p>"
}
```
### 7. [slice](https://angular.io/api/common/SlicePipe)
- substring
- `<a [href]="article.href">{{ article.title | lowercase | slice:0:20 }}</a>  // output substring and display from char[0] to character before char[20], i.e. char[19]`
- `<a [href]="article.href">{{ article.title | lowercase | slice:-1:-20 }}</a>  // output let str has 30 characters, substring from char[29-20], i.e. char[19] to character before char[29-1], i.e. char[28]`
- pagination
- 6 articles, show articles[0] to before articles[2], i.e. articles[1] 
- `<article class="post" id="post{{idx}}" *ngFor="let article of articles | slice:0:2; let idx=index">`

## E. [safe navigation operator](https://angular.io/guide/template-expression-operators)

- vscode: multiple select, mark keyword e.g.`"title"`, then `Ctrl + D`
- 
```
<a [href]="article.href">{{ article.subject.title | lowercase | slice:0:20 }}</a>
```
- may result in `core.js:4196 ERROR TypeError: Cannot read property 'title' of undefined`

- `?.`: if object not null, get its child; if object is null, return null.
```
<a [href]="article.href">{{ article.subject?.title | lowercase | slice:0:20 }}</a>
<small>{{ article.subject?.subtitle }}</small>
```

### avoid error type of variable in TypeScript
- traditional, same as above
```
<a [href]="article.href">{{ article['subject']?.title | lowercase | slice:0:20 }}</a>
<small>{{ article['subject']?.subtitle }}</small>
``` 

- fashioned, same as above
```
<a [href]="article.href">{{ $any(article).subject?.title | lowercase | slice:0:20 }}</a>
<small>{{ $any(article).subject?.subtitle }}</small>
```