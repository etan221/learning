## Kong on cloud

Kong Cloud what is new vs Kong on premise.

1.	API Endpoint changes
2.	Quick technical drive on changes in Kong current (on premise version) VS Kong latest version
3.	Developer portal 
    a.	How it replaces API explorer
    b.	Access to developer portal (internal / external clients)   
4.	Consumer tracking and approval flow 
5.	Restrictions 
    a.	No Access to UI
    b.	Use of infrastructure as code
    c.	Automatic gateway configuration on deployment of API / Integrations
6.	What things would be decommissioned 
    a.	Kong on-premis
    b.	API explorer
    c.	Auth Server
7.	Migration process / strategy 

