## Authentication

### AWS Cognito Tutorial 
1. [Cognito User Pool & AWS Amplify setup](https://www.youtube.com/watch?v=EaDMG4amEfk), [Amplify](https://docs.amplify.aws/)
2. [Sign in & registration](https://www.youtube.com/watch?v=2SaO1Pvah2k)
3. [Session persistence & change password](https://www.youtube.com/watch?v=-K85GjI5SQ0)

### OAuth
- [OAuth 2.0: An Overview](https://www.youtube.com/watch?v=CPbvxxslDTU)
- [OAuth 2.0 and OpenID Connect](https://www.youtube.com/watch?v=996OiexHze0)

- [Delegated authorization problem] (https://youtu.be/996OiexHze0?t=569)
    - how can I let a website access my data (without giving it my password)?
    - I trust gmail and I kind of trust Yelp. I want Yelp to have acess to my contacts only.
    - I trust **UOA** and I kind of trust **RBS**. I want RBS to have access to my **?information in UOA**.

- [Delegated authorization with OAuth 2.0](https://youtu.be/996OiexHze0?t=921)
1. in yelp.com you click button *connect with google*
1. in accounts.google.com, you login with username password
1. in accounts.google.com, prompt: allow yelp to access your public profile and contacts? and you click yes
1. in accounts.google.com, redirect/callback *http://www.yelp.com/callback*
1. in yelp.com, it shows *loading...* on the frontend, while accessing contacts.google.com in the backend

1. goto RBS.com
1. (skipped) in RBS you click button *connect with uoa/cognito/shibolith*
1. in uoa/cognito/shibolith, you login with username password
1. (skipped) in uoa/cognito/shibolith, prompt: allow RBS to access your public profile and contacts? and you click yes
1. in uoa/cognito/shibolith, redirect/callback *http://www.RBS.com/callback*
1. in RBS, it shows *loading...* on the frontend, while accessing contacts.(uoa/cognito/shibolith) in the backend


                                    B1. goto RBS.com
A1. User directed to authenticate = B2. (skipped) in RBS you click button *connect with uoa/cognito/shibolith*
                                    B3. in uoa/cognito/shibolith, you login with username password
A2. Determine IdP                 = B4. (skipped) in uoa/cognito/shibolith, prompt: allow RBS to access your public profile and contacts? and you click yes
A3. Redirect to IdP
A4. User autenticated by IdP (SSO if active session)
A5. POST back to AWS Cognito      = B5. in uoa/cognito/shibolith, redirect/callback *http://www.RBS.com/callback*
A6. Create/UPdate profile         = B6. in RBS, it shows *loading...* on the frontend, while accessing contacts.(uoa/cognito/shibolith) in the backend
A7. AWS Cognito tokens provided to app

Eric Tan's zoom account
https://auckland.zoom.us/j/9473347749 


- [OAuth 2.0 Terminology](https://youtu.be/996OiexHze0?t=972) 
    - resource owner (you, who own the data and can click to allow someone access)
    - client (yelp.com)
    - authorization server (accounts.google.com)
    - resource server (contacts.google.com, hold the resource)
    - authorization grant (prove that you have clicked yes to grant access)
            - type of authorization grant: authorization code grant
    - redirect URI (redirect back to the callback URI, where to go after you clicked yes, i.e. yelp.com/callback)
        - back to redirect URL with authorization code
        
           - redirect URI may exchange authorization code for access token
    - access token (key to access the resource)
            - talk to resource server with access token
            
    - back channel - highly secure channel
        -   server code, goto google api, my backend to some other system
    - front channel - less secure channel
        -   from browser, secret password, 

- [authorization flow with code](https://youtu.be/996OiexHze0?t=1491)
    - response type: code
    - scope: profile contacts
    
- [Http Request](https://youtu.be/996OiexHze0?t=2049)
```url
https://accounts.google.com/o/oauth2/v2/auth?
client_id=abc123&
redirect_uri=https://yelp.com/callback&
scope=profile&
response_type=code&
state=foobar
```
- [OauthDebugger.com](https://youtu.be/996OiexHze0?t=2260)
    - https://oauthdebugger.com/

- [Calling back](https://youtu.be/996OiexHze0?t=2376)

- Denied
```
https://yelp.com/callback?
error=access_denied&
error_description=The user did not consent.
```
- Approved
```
https://yelp.com/callback?
code=oMsCeLvIaQm6bTrgtp7&
state=foobar
```

- [Exchange code for an access token](https://youtu.be/996OiexHze0?t=2404)
```
POST www.googleapis.com/oauth2/v4/token
Content-Type: appliction/x-www-form-urlencoded

code=oMsCeLvIaQm6bTrgtp7&
client_id=abc123&
client_secret=secret123&
grant_type=authorization_code
```

- [Authorization server returns an access token](https://youtu.be/996OiexHze0?t=2428)
```
{
    "access_token": "fFAGRNJru1FTz70BzhT3Zg",
    "expires_in": 3920,
    "token_type": "Bearer",
}
```

- [Use the access token](https://youtu.be/996OiexHze0?t=2444)
```
GET api.google.com/some/endpoint
Authorization: Bearer fFAGRNJru1FTz70BzhT3Zg 
```

- API
- i.e. contacts.google.com
- validate token
- use token scope for authorization
 
- [Various OAuth Flows/Grant Types](https://youtu.be/996OiexHze0?t=2510)
    - Authoriztion code (F+B)
        - return code, then exchange with a token
        - `response type: code`
    - Implicit (F)
        - return token right away, applications with no backend
        - `response type: token`
        
    - Client credentials (B)
        - server-to-server, return token right away
    - Resource owner password credentials (B)
    
    - F: frontend channel, browser based, not that secure channel e.g. url redirect
    - B: backend channel, server based, secure channel
    
- [Identity use cases (pre-2014)](https://youtu.be/996OiexHze0?t=2743)
    - Simple login (authn)
    - Single sign-on across sites (authn)
    - Mobile app login (authn)
    - Delegated authorization (authz)
    
    - authz: authorization - can you access? => OAuth
    - authn: authentication - who you are? => OpenId
    
    **OAuth is bad for authentication, it does not care who you are**
    
 - [OpenID Connect](https://youtu.be/996OiexHze0?t=2939)
    - OpenID Connect is for authentication
    - OAuth 2.0 is for authorization

| OpenID Connect |
|---|
|OAuth 2.0|
|HTTP|

- [What OpenID Connect adds](https://youtu.be/996OiexHze0?t=2998)
    - ID token
        - contains ID, and information about the user
    - UserInfo endpoint
        - for getting more user information
        - not just ask for an access token, but and openId token 
    - Standard set of scopes
    - Standardizard implementation
   
- request: it is both an openId request and an OAuth request 
```
response code: code
scope: openid profile
``` 
- [**exchange authorization code for**](https://youtu.be/996OiexHze0?t=3123)
    1. access token and 
    2. ID token
    
- [demo](https://youtu.be/996OiexHze0?t=3166)
   - https://oidcdebugger.com/
  
- request 
```
https://dev-341607.oktapreview.com/oauth2/aus9o0wvkhockw9TL0h7/v1/authorize?
client_id=lXSenLxxPi8kQVjJE5s4
&redirect_uri=https://oidcdebugger.com/debug
&scope=openid
&response_type=id_token
&response_mode=fragment
&state=foobar
&nonce=jtwnl4xuvk
```
- return with token, Json Web Token (JWT), because the request run in implicit flow as well 
```
https://oidcdebugger.com/debug#id_token=ld1jdlajdlasu123i1u2eoiqud1ovdf7d...
```

- decode token
- https://www.jsonwebtoken.io

- JWT String
```
eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWUsImp0aSI6IjE1MDQ5YzBhLTdiODctNDIzNS04NWUyLTQ0Yzk2MTg3YjJjOCIsImlhdCI6MTU5ODIyMTAyNiwiZXhwIjoxNTk4MjI0NjI2fQ.h23ldHp67QAdOINPCZE1kXZvFj84oB7Jw2iVOH4vceU
```
- Header
```
{
 "typ": "JWT",
 "alg": "HS256"
}

```
- Payload / Claims
```
{
 "sub": "1234567890",
 "name": "John Doe",
 "admin": true,
 "jti": "15049c0a-7b87-4235-85e2-44c96187b2c8",
 "iat": 1598221026,
 "exp": 1598224626
}
```

```
{
 "iss": "https://accounts.google.com",
 "sub": "you@gmail.com",
 "name": "Nate Barbetini",
 "aud": "s6BhdRkqt3",
 "exp": 1311281970,
 "iat": 1311280970,
 "auth_time": 111280969
}
```
- Signature: stamp of the content

- [Calling the userInfo endpoint](https://youtu.be/996OiexHze0?t=3352)
```
GET www.googleapis.com/oauth2/v4/userinfo
Authorization: Bearer fFAGRNJru1FTz70BzhT3Zg

200 OK
Content-Type: application/json

{
    "sub": "you@gmail.com",
    "name": "Nate Barbettini",
    "profile_picture": "http://plus.g.co/123"
}
```

- [Identity use cases (today 2018)](https://youtu.be/996OiexHze0?t=3380)
    - Simple login (OpenID Connect for authn)
    - Single sign-on across sites (OpenID Connect for authn)
    - Mobile app login (OpenID Connect for authn)
    - Delegated authorization (OAuth 2.0 for authz)
    
- [OAuth and OpenId Connect](https://youtu.be/996OiexHze0?t=3407)
    - Use OAuth 2.0 for Authorization
        - granting access to your API
        - getting access to user data in other systems
    - Use OpenId for Authentication
        - Logging the user in
        - Making your accounts available in other systems
        
- Practical: [Which grant type (flow) do I use?](https://youtu.be/996OiexHze0?t=3444)
    - web application w/ server backend: authorization code flow
    - native mobile app: authoirzation code flow with PKCE
    - JavaScript app (SPA) w/ API backend implicit flow
    - Microservices and APIs: client credential flow
    
- https://developer.okta.com/


### AWS Cognito
 - identity provider and user directory
 - for developer to add sign-in, registration, sign-out functionality to their applications
 - cognito provider identity token, handle authorization back and forth
 - user pool as database of users
 - use [aws amplify](https://docs.amplify.aws/) to implement sign-in, sign-out, registration
 
 - [aws-cognito-tutorial-starter](https://github.com/jspruance/aws-cognito-tutorial-starter)
   - no need to manually refresh token
 
### [AWS Cognito Login page (SSO) set up with Angular 9 application](https://www.youtube.com/watch?v=fOWy0fDBmGg)

### [RBS Access control](https://wiki.auckland.ac.nz/pages/viewpage.action?spaceKey=ITPRO&title=Access+Control)
