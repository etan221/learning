## Architecture

1. [AWS Serverless API - Basic](https://wiki.auckland.ac.nz/display/ITCB/AWS+Serverless+API+-+Basic)
    - Andy: Some info on the most basic API you can possibly make:
    - ...and more info here: https://www.serverless.com/
    - We can help you get set up if you need. (edited)
    - Example: 
        - Solution Design: [Wayfinding](https://wiki.auckland.ac.nz/display/UoASA/Wayfinding) app
            - A bit simpler than your app, but the technology is exactly the same: Angular frontend and API Gateway + Lambda + DynamoDB backend.
        - Serverless: [Wayfinding Backend](https://bitbucket.org/uoa/uoa-wayfinding-backend/src/master/)
            - Once you've got familiar with how Amazon API Gateway + Lambda work together, we recommend using Serverless framework to define your backend in code.
     
2. Hosting: [AWS Host S3 Static Web Content](https://wiki.auckland.ac.nz/pages/viewpage.action?spaceKey=ITCB&title=AWS+Host+S3+Static+Web+Content)
    - Nicolas: I hope you are a bit familiar with cloud technologies like AWS. If not, no worries, but maybe before going to deep into our dedicated wiki pages, it could be great for you to watch YouTube videos as introduction. Key words would be “AWS introduction”, “Single Page Application with Angular”, “hosting application on S3 bucket”
 
3. Authn/Authz: [SPA Openid-Oauth2.0 authentication and authorization with AWS Cognito](https://wiki.auckland.ac.nz/display/ITCB/SPA+Openid-Oauth2.0+authentication+and+authorization+with+AWS+Cognito)
    - Andy: more info on calling API’s from a web app. The authentication should be handled by the auth library within the Ionic base application (with a little config – see the readme).

4. Data: [Data Hub - How to consume Data Hub data](https://wiki.auckland.ac.nz/display/IAS/Data+Hub+-+How+to+consume+Data+Hub+data)
    - Andy: More info on integrating with Data Hub:


## Example:
- Ionic/Angular:  [ionic-shell](https://bitbucket.org/uoa/ionic-shell-application/src/master/)
    -   It is our starter kit for making web applications with Angular. Yes, it utilises Ionic framework on top of Angular, but this doesn’t necessarily mean it needs to be published as a mobile app.


------------------------------
Good morning Eric,

We will be back to you soon, but in the meantime I can give you some homework 😃

For your questions I think you can find most of the answers here:
1. [AWS Host S3 Static Web Content](https://wiki.auckland.ac.nz/pages/viewpage.action?spaceKey=ITCB&title=AWS+Host+S3+Static+Web+Content)
2. [SPA Openid-Oauth2.0 authentication and authorization with AWS Cognito](https://wiki.auckland.ac.nz/display/ITCB/SPA+Openid-Oauth2.0+authentication+and+authorization+with+AWS+Cognito)

Have fun 😊

Will be in touch soon.

-- 
Nicolas Dreno
Digital Solutions Architect
Level 5 - 58 Symonds Street (435)
+64 272 024 398

Find me on Zoom
https://auckland.zoom.us/my/nicolasdreno

---------------------------


Hi Nicolas,

1.	AWS is new to me. I googled the keywords. Kindly please correct me.

Angular - TypeScript-based open-source web application framework (our Frontend)

Ionic  - Ionic Framework is an open-source mobile UI toolkit for building high quality, cross-platform native and web app experiences. Move faster with a single codebase, running everywhere. (a toolkit for wrapping web app and make it a mobile native app)

Brett, will this project need a mobile frontend on top of a web applicaiton?


AWS S3 - Amazon Simple Storage Solution(S3) is object storage built to store and retrieve any amount of data from anywhere on the Internet. It’s a simple storage service that offers an extremely durable, highly available, and infinitely scalable data storage infrastructure at very low costs. (for hosting static files/data, like html,css,js)

AWS Cloudfront - Amazon CloudFront is a fast content delivery network (CDN) service that securely delivers data, videos, applications, and APIs to customers globally with low latency, high transfer speeds, all within a developer-friendly environment. (CDN for fast delivery of files that stored in S3)

AWS Route 53 - Amazon Route 53 is a highly available and scalable cloud Domain Name System (DNS) web service. It is designed to give developers and businesses an extremely reliable and cost-effective way to route end users to Internet applications by translating names like www.example.com into the numeric IP addresses like 192.0. (DNS, so our application could be found by user/other system)


2.	Regarding authentication/authorization and signing patterns, could you please point me some links for reference?

3.	Regarding integration hub, do you mean Integration By Convention + Kafka?

4.	For AWS, authn/authz/signing patterns, and integration, are there any sample code or application or environment that I can refer to? 

Best Regards

Eric

M: 027 627 1747

-----------------------------------

Hi Eric

No worries.

In addition to the Wiki articles Nico shared, you should also take a look at this repository:
https://bitbucket.org/uoa/ionic-shell-application/src/master/

It is our starter kit for making web applications with Angular. Yes, it utilises Ionic framework on top of Angular, but this doesn’t necessarily mean it needs to be published as a mobile app.

Here is some more info on calling API’s from a web app:
https://wiki.auckland.ac.nz/pages/viewpage.action?pageId=173769037

The authentication should be handled by the auth library within the Ionic base application (with a little config – see the readme).

More info on integrating with Data Hub:
https://wiki.auckland.ac.nz/display/IAS/Data+Hub+-+How+to+consume+Data+Hub+data

We have just begun reviewing the requirements and it looks like a backend will need to be developed for this app too.

More info to come as we work our way through the design.

In the meantime, please get in touch if you have an queries about any of the above.

You can find us on Slack if that’s easier :)

Thanks
Andy

-------------------------------------