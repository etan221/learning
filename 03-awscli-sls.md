## AWS CLI on Windows

### A. AWS CLI with my own AWS account
- [How to Install and Configure AWS CLI in Windows](https://www.youtube.com/watch?v=n3KacV0UlSM)

#### 1. Install AWS CLI
- https://aws.amazon.com/cli/

`aws --version`

```
aws-cli/2.0.35 Python/3.7.7 Windows/10 botocore/2.0.0dev39
```
#### 2. Get Security Credential
- https://console.aws.amazon.com/iam/home?#/users/etan221_dev?section=security_credentials

`sls config credentials --provide aws --key 123 --secret XYZ`

`aws configure`

```
AWS Access Key ID [****************77VL]: 123
AWS Secret Access Key [****************WksU]: XYZ
Default region name [None]: ap-southeast-2
Default output format [None]: json / table / text
```
#### 3. Test your access

`aws s3 ls`

```
2020-07-29 17:03:55 sls-dev-serverlessdeploymentbucket-mcq1p3e54vss
```
#### 4. [AWS CLI Tutorial](https://youtu.be/JscvSRcFbuA?t=464)

`aws help`

`aws iam help`

`aws iam list-groups`

`aws iam list-users`

`aws dynamodb help`

`aws dynamodb list-tables`

`aws dynamodb describe-table --table-name posts`

`aws lambda list-functions`

`aws lambda get-policy --function-name sls-dev-getAllPosts`

`aws lambda get-account-settings`

`aws s3 help`

- Serverless Framework Tutorial
    - [Introduction](https://www.youtube.com/watch?v=lUTGk64jppM&list=PLzvRQMJ9HDiT5b4OsmIBiMbsPjfp4kfg3)
    - [configure AWS credentials](https://www.youtube.com/watch?v=tgb_MRVylWw)
    - [Serverless test locally](https://www.youtube.com/watch?v=ul_85jfM0oo)
    - [Environment Variable](https://www.youtube.com/watch?v=1_sOPBgRAww)

### B. AWS CLI with AWS Temoporary Credentials of UOA AWS account
- [AWS Temporary Credentials for CLI](https://wiki.auckland.ac.nz/pages/viewpage.action?spaceKey=UC&title=AWS+Temporary+Credentials+for+CLI)
```
C\Users\etan221> del .gitconfig
```

`git config --global user.email "eric.tan@auckland.ac.nz"`

`git config --global user.name "etan221"`

```
C:\Users\etan221\.ssh> [copy id_rsa and id_rsa.pub from ubuntu]
```

`del know_hosts`

#### 1. Setup environment for Python 3 (Python 2 is EOL)

- [Install Python 3.7.4](https://www.youtube.com/watch?v=4Rx_JRkwAjY)
    - `C:\Python\Python38` or `C:\Users\etan221\AppData\Local\Programs\Python\Python37`
      -  script works for Python 3.6 and 3.7. Other versions may be missing the relevant packages
    - Add to PATH
      - `C:\Users\etan221\AppData\Local\Programs\Python\Python37\`: python
      - `C:\Users\etan221\AppData\Local\Programs\Python\Python37\Scripts`: pip
    - test Python with IDLE (IDE)/Python CLI
    - test Python in Window Command Prompt

`python --version`

```
Python 3.8.5
```

`python`

```
Python 3.8.5 (tags/v3.8.5:580fbb0, Jul 20 2020, 15:57:54) [MSC v.1924 64 bit (AMD64)] on win32
Type "help", "copyright", "credits" or "license" for more information.
>>> print("Hello World")
Hello World
>>> quit()
```

`pip --version`

```
pip 20.1.1 from c:\python\python38\lib\site-packages\pip (python 3.8)
```

#### 2. Clone the utility repo

`git clone git@bitbucket.org:uoa/utility-aws-cli-access.git`

`cd utility-aws-cli-access`

#### 3. Install dependencies

`pip install -r requirements.txt`

***
- a problem could be fixed by using MarkupSafe==1.1.1 in requirement.txt**, [ref](https://github.com/pallets/markupsafe/issues/116)
***

#### 4. Generate temporary credential

`cd C:\opt\uoa_project\utility-aws-cli-access`

modify aws_saml_login.py, replace `saml` with `uoa`

```
parser.add_argument('--profile', default='saml', help='The profile to create in the credentials file, defaults to uoa')
```
uncomment to view more information
```
...
print(saml_response)
...
print(awsroles)
...
```

`python aws_saml_login.py --idp iam.auckland.ac.nz --profile uoa-sandbox`

```
Password:
Multi-factor token:
A temporary credential has been obtained for accessing AWS.
To use this credential, call the AWS CLI with the --profile uoa-sandbox option.
The token will expire at 2020-08-02 23:15:40+00:00.
After this time, you may safely rerun this script to refresh your access key pair.
```

- if multiple roles found 
``` 
UoA ITS Non Prod-devops
UoA Sandbox-training
```
- use this:
`python aws_saml_login.py --idp iam.auckland.ac.nz --account training --profile uoa-sandbox`

- **`aws_saml_login.py` is equivalent to this step: How can I use the AWS CLI to call and store SAML credentials? [video](https://youtu.be/ACWRmFjG9V8?t=223), [text](https://aws.amazon.com/premiumsupport/knowledge-center/aws-cli-call-store-saml-credentials/)**
#### 4. verify aws profile

`cat C:\Users\etan221\\.aws\config`

```
[default]
region = ap-southeast-2
output = json
[profile uoa-sandbox]
region = ap-southeast-2
output = json
```

`cat C:\Users\etan221\\.aws\credentials`
```
[default]
aws_access_key_id = AKIAVKAAPXUSGHVS
aws_secret_access_key = A6a+BWyB1ct4B+Eg3wPUpgy52ksk7RddZnkS

[uoa]
aws_access_key_id = ASIAWB6X7CJWEOC7CES2
aws_secret_access_key = Yq9isAnhm9W7ylSA3hevc6ta2VsXwPzTQxr21Rbc
aws_session_token = FwoGZXIvYXdzEM3//////////wEaDL1M4XnM4SjstmhOLSLLAl8hfeUjcvvXMDi/jCtTnNUeKEP2eS+jJoIgJgKfqJHZiOYIaU1NrpV0+3d4titfAGR7AysEv28u9kXeYqx4J9Kq8Aj9aHfuK+7TjL3WeQlsVWD+VWLLKC+Z4bnVLtks8TaaI97Ksn/ChwQ5Z8efJFeRfsxwluvFLUw/zJVunBii1Rw2aqZkA3/qqtgbSebfNTAeYhYyJKWlIm7tzeYlif6gA5naoUkE/0et7fV7h2QXxYwPgRENPPeN+r9kY3xS1I5wcti+QCg/20XxVUyHm65la1HxuOB290lTJJHLa3PqrAsUNp7aeM94eYFYovf2qSgHorDzphXQt7PfxroksaArRo0vj6Qaim+S/VN+yHYcIUeWJfvrbl9WlOidsFZm33jSGH+fIRMU5XSGT7fWCpjqmU+qrMylsWK7NzBUNPQ2Xz8IhYydkmttwKgozYie+QUyM3+iiQBSx2opj9BPMK3+jXx+kKT1SiK1VsSUR8OAdCkNmo6nYa4Z/HVWWyWHutvMyzgpYQ==
```

`aws configure list-profiles`
```
default
uoa
```

#### 5. Use the UOA credential

`aws s3 ls`

```
2020-08-03 14:41:39 hello-api-dev-serverlessdeploymentbucket-196hnl868k0gb
2020-07-29 17:03:55 sls-dev-serverlessdeploymentbucket-mcq1p3e54vss
```

`aws s3 ls --profile uoa-sandbox`

```
2020-06-17 05:15:35 416527880812-default-trail
2020-06-17 05:15:36 416527880812-sagemaker-ap-southeast-2
2020-06-17 05:15:35 416527880812s3accesslogsap-southeast-2
2020-06-30 07:49:20 416527880812s3accesslogsus-east-1
2020-02-18 08:18:14 aabe511-ce-poc-01
2019-09-27 14:58:49 aabe511-game-data
...
```
- Note: Do note that these credentials have a limited lifecycle, so you may need to be careful about running longer tasks when they are due to expire. This may have unexpected results if you don't have continuing access during a run.
```
An error occurred (ExpiredToken) when calling the ListUsers operation: The security token included in the request is expired
```
### C. Serverless framework - [create](https://youtu.be/jZ-AdfA618U?list=PLzvRQMJ9HDiT5b4OsmIBiMbsPjfp4kfg3&t=70), [deploy, invoke](https://www.youtube.com/watch?v=c1D1Ev0qA7k)

#### 1. install

`npm install serverless -g`

`npm install serverless-domain-manager -g`

#### 2. create project

`mkdir hello-api`

`cd hello-api`

`sls create --template aws-nodejs`

##### serverless.yml
```yml
service: hello-api

provider:
  name: aws
  runtime: nodejs12.x
  stage: dev
  region: ap-southeast-2

functions:
  hello:
    handler: handler.hello
```

##### handler.js
```js
'use strict';

module.exports.hello = (event, context, callback) => {
  const response = {
    statusCode: 200,
    body: JSON.stringify({
        message: 'Go Serverless v1.0! Your function executed successfully!',
        input: event,
      }),
  };
  
  callback(null, response);
};
```
#### 3. deploy
- **[deploy Serverless to multiple account](https://seed.run/blog/how-to-deploy-your-serverless-app-into-multiple-aws-accounts.html)**

`sls deploy`

`sls deploy --aws-profile uoa-sandbox`

#### 4. verify

`aws cloudformation list-stacks`

`aws cloudformation list-stacks --profile uoa-sandbox`

`aws lambda list-functions`

`aws lambda list-functions --profile uoa-sandbox`

`aws lambda get-function --function-name hello-api-dev-hello`

`aws lambda get-function --function-name hello-api-dev-hello --profile uoa-sandbox`

#### 5. invoke

`sls invoke --function hello`

`sls invoke --function hello --aws-profile uoa-sandbox`

```
{
    "statusCode": 200,
    "body": "{\"message\":\"Go Serverless v1.0! Your function executed successfully!\",\"input\":{}}"
}
```
#### 5. invoke locally

`sls invoke local --function hello`
**depends on aws service, lambda/dynamodb can run locally, while scs/s3 cannot**

#### 6. undeploy

`sls remove`

`sls remove --aws-profile uoa-sandbox`
