## Protractor - e2e test

### [Unit Testing: Jasmine, Protractor, Karma](https://www.youtube.com/watch?v=D6qPDww2X8k)
 - Jasmine
    - behaviour driven development
    - it structures our test code
 - Protractor
    - run test against browser, no wait nor sleep
    - [protractor.conf.js](https://github.com/angular/protractor/blob/master/lib/config.ts)
 - Karma
    - runner, on multiple browsers
    - [karma.conf.js](https://karma-runner.github.io/1.0/config/configuration-file.html)
    
 - integrate three: code in Jasmine, run in Protractor while Karma helps run testing code in multiple browsers

### [Angular 5 e2e Testing with Protractor Tutorial](https://www.youtube.com/watch?v=WROmnhRbH6k), [source](https://github.com/techsithgit/ng5-create-routes)
- `npm install`
- `ng serve --open`
- `Cannot find module '@angular-devkit/core'.`
- `npm update -g @angular/cli`
- change `@angular/cli": "1.6.0",` to `@angular/cli": "^1.6.0"`
- `npm update`
- `ng e2e`

### 1. [Protractor Tutorial](https://www.youtube.com/watch?v=p8ENoeZENhk&list=PLhW3qG5bs-L_dgIr3hiOlnNIO8NGlXQnP)
- Protractor is a nodejs program and a wrapper of [WebDriverJS](http://www.webdriverjs.com/protractor/)
- WebDriverJS is the official javascript implementation of selenium
- WebDriverJS helps to interact with elements on the web
- Protractor extends WebDriveJS functionalities to enable automation of use actions on browser applications
- ![protractor architecture](https://i1.wp.com/www.webdriverjs.com/wp-content/uploads/2016/12/Protractor-Architecture.png?resize=273%2C300)
- Protractor runs tests against your application running in a real browser, interacting with it as a user would
- *Protractor can also be used to test non-angular applications*

### 2. [Why Protractor](https://www.youtube.com/watch?v=7Urc5cSIdLg&list=PLhW3qG5bs-L_dgIr3hiOlnNIO8NGlXQnP&index=2)
- web application has multiple elements: links, text boxes, drop down, checkboxes, radio buttons, frames, tables
- all elements has properties: id, name, class, text
- locate elements by properties: xpath e.g. google search box: `//input[@id='realbox']`, css path
- angular has specific properties: ng-repeater, ng-controller, ng-model
- **protractor is a wrapper over selenium webdriverjs and provides support to find angular specific elements**
- **synchronization issues while automating Angular applications with Selenium webdriver**
- **[protractor style guide](https://www.protractortest.org/#/style-guide)**

### 3. [How Protractor works](https://www.youtube.com/watch?v=GZmAcwkV6BI&list=PLhW3qG5bs-L_dgIr3hiOlnNIO8NGlXQnP&index=3)
- Test scripts
    - webdriverjs is the javascript binding for the selenium webdriver api
    - protractor is a wrapper around webdriverjs. it can use functions of webdriverjs and has additional features to handle angular elements and actions
    - both of the above are nodejs applications. nodejs is a javascript runtime environment to build and run server-side network applications.

- Server
    - Selenium Server can run locally or remotely
    - Test scripts can communicate directly with Chrome Driver or Firefox Driver, without use of Selenium Server. in you config file set `directConnect: true`.
    
- Browser
    - Browser drivers: Chromedriver.exe, Geckodriver.exe
    - Angular web application wrapped by a browser
    
1. Test scripts send commands to the Selenium Server
    - on execution, test commands go to selenium server.
2. Selenium Server interacts with Browser Drivers
    - selenium server interprets these commands, then send commands to browser drivers via JSON Wire protocol --> [How Selenium works](https://www.youtube.com/watch?v=ynJt8E3y6C4&list=PLhW3qG5bs-L_KRbwPdatLfL-rRfE2N9TX&index=3&t=0s)
3. Browser Drivers interact with application on the real browser
    - browser drivers interprets these commands, execute commands on application which is running on real browsers
    
protractor runs extra commands to ensure the application has stabilized
- `element(by.css('button.myclass')).click();`
This will result in 3 commands being sent to browser driver
- `/session/:sessionId/execute_async` - ask if Angular is ready, is done with all timeout and asynchronous requests
- `/session/:sessionId/element` - find the elements
- `/session/:sessionId/element/:id/click` - perform click

Reference:
- [How Protractor works](https://www.protractortest.org/#/infrastructure)
- [Protractor API](https://www.protractortest.org/#/api?view=ProtractorBrowser.prototype.findElement)
- [Setting up Selenium Server](https://www.protractortest.org/#/server-setup)
- [Browser support](https://www.protractortest.org/#/browser-support)
- [Browser setup](https://www.protractortest.org/#/browser-setup)
 
### 4. [How to setup Protractor on windows](https://www.youtube.com/watch?v=Dmu_vP3N3Vg&list=PLhW3qG5bs-L_dgIr3hiOlnNIO8NGlXQnP&index=4)
step 1: install node.js
- `node -v`
- `npm -v`
step 2: install protractor
- `npm install -g protractor`
- `protractor --version`
- `where protractor` -> `C:\Users\etan221\AppData\Roaming\npm\protractor`
step 3: update webdriver
- `webdriver-manager update` -> `C:\Users\etan221\AppData\Roaming\npm\node_modules\protractor\node_modules\webdriver-manager\selenium`
step 4: find conf.js file at `C:\Users\etan221\AppData\Roaming\npm\node_modules\protractor\example`
```
// An example configuration file.
exports.config = {
  directConnect: true,

  // Capabilities to be passed to the webdriver instance.
  capabilities: {
    'browserName': 'chrome'
  },

  // Framework to use. Jasmine is recommended.
  framework: 'jasmine',

  // Spec patterns are relative to the current working directory when
  // protractor is called.
  specs: ['example_spec.js'],

  // Options to be passed to Jasmine.
  jasmineNodeOpts: {
    defaultTimeoutInterval: 30000
  }
};
```
step 5: Run protractor: example
- `cd C:\Users\etan221\AppData\Roaming\npm\node_modules\protractor\example`
- `protractor conf.js`
```
[22:05:23] I/launcher - Running 1 instances of WebDriver
[22:05:23] I/direct - Using ChromeDriver directly...

DevTools listening on ws://127.0.0.1:51593/devtools/browser/eaaab5a8-dd89-4acd-b357-6774199e03d2
Started
...


3 specs, 0 failures
Finished in 7.342 seconds

[22:05:33] I/launcher - 0 instance(s) of WebDriver still running
[22:05:33] I/launcher - chrome #01 passed
```

*5. setup in mac*
### 6. [Creating Protractor Framework on Visual Studio Code from scratch](https://www.youtube.com/watch?v=nk_95kK7TSY&list=PLhW3qG5bs-L_dgIr3hiOlnNIO8NGlXQnP&index=6)
- `mkdir protractor-demo`
- `cd protractor-demo`
- `mkdir conf`
- `mkdir tests`
- etan221@it369948 MINGW64 /c/opt/e2e_test/protractor-demo
- `cp /c/Users/etan221/AppData/Roaming/npm/node_modules/protractor/example/conf.js conf/.` 
- `cp /c/Users/etan221/AppData/Roaming/npm/node_modules/protractor/example/example_spec.js tests/.`
- `code .`
- see video for tips to setup
- vscode plugin: code runner

### 7. [Create first test case in protractor](https://www.youtube.com/watch?v=u_awobQ1fws&list=PLhW3qG5bs-L_dgIr3hiOlnNIO8NGlXQnP&index=7)
- create a test file
- add steps in test file
- add element locators
- run test

- conf.js - configuration: browser, framework
- spec.js - tests are present in spec file

- [protractor demo application](https://www.protractortest.org/#/tutorial)
- option: chome extension: whatruns, wappalyzer, tells what web technologies the website is using
- run protractor test on an [angular demo application](http://juliemr.github.io/protractor-demo/.)
- vscode snipplet: `t-describe-it`, `t-it` generate a block of Jasmine test code

- [Protractor API](https://www.protractortest.org/#/api)
- `get` - [ProtractorBrowser.prototype.get](https://www.protractortest.org/#/api?view=ProtractorBrowser.prototype.get)
- inspect:`ng-model="first"` - [ng-model](https://www.protractortest.org/#/api?view=ProtractorBy.prototype.model)
- `sendkey` - [webdriver.WebElement.sendKeys](https://www.protractortest.org/#/api?view=webdriver.WebElement.prototype.sendKeys)
- `css` - [element](https://www.protractortest.org/#/api?view=ElementFinder.prototype.element)
- `click` - [element(locator).click](https://www.protractortest.org/#/api?view=webdriver.WebElement.prototype.click)
- inspect:`class="ng-binding"` - [cssContainingText](https://www.protractortest.org/#/api?view=ProtractorBy.prototype.cssContainingText)

- [Jasmine cheatsheet](https://devhints.io/jasmine)
- vscode plugin: protractor snipplet, `bg` -> 

### 8. [How to find element locators](https://www.youtube.com/watch?v=I139ptfCEtg&list=PLhW3qG5bs-L_dgIr3hiOlnNIO8NGlXQnP&index=8)
1. How to find elements on webpage for Angular applications
2. Types of locators
3. Locator strategies
4. Demo
5. Tips & Tricks
- Finding elements on webpage and taking action on them
- Protractor exposed 2 global function
 - element -> find a single element
 - element.all -> find multiple elements
 - `element(by.model('first')).sendKeys('3')`
 
- [Protractor locators](https://www.protractortest.org/#/locators)
- chrome plugin: [pom locator](https://youtu.be/I139ptfCEtg?list=PLhW3qG5bs-L_dgIr3hiOlnNIO8NGlXQnP&t=462), right click POM builder, suggested locator, get `[ng-model='first']`
- chrome plugin: [Protractor Recorder](https://youtu.be/I139ptfCEtg?list=PLhW3qG5bs-L_dgIr3hiOlnNIO8NGlXQnP&t=573)
- chrome plugin: [Selenium IDE](https://youtu.be/I139ptfCEtg?list=PLhW3qG5bs-L_dgIr3hiOlnNIO8NGlXQnP&t=688)
- Protractor [Cheat Sheet](https://youtu.be/I139ptfCEtg?list=PLhW3qG5bs-L_dgIr3hiOlnNIO8NGlXQnP&t=800): [api](https://gist.github.com/javierarques/0c4c817d6c77b0877fda), [example](https://gist.github.com/julekgwa/28ecfc93f3998b4302e2653bf43ac4ed)

- [Best Practise](https://www.logigear.com/blog/test-automation/15-best-practices-for-building-an-awesome-protractor-framework/)

### 9. [Page Object Model](https://www.youtube.com/watch?v=RSyDesxeXik&list=PLhW3qG5bs-L_dgIr3hiOlnNIO8NGlXQnP&index=9)
1. What is POM
2. How to implement POM in protractor framework
3. Demo

1. We separate objects/element locators and actions in separate files
2. We create one file for each web page
3. Test scripts can refer element locators and actions from these files

- pages/homepage.js
```
let homepage = function () {
    let firstNumber_input = element(by.model('first'));
    let secondNumber_input = element(by.model('second'));
    let goButton = element(by.css('[ng-click="doAddition()"]'));

    this.get = function (url) {
        browser.get(url);
    };

    this.enterFirstNumber = function (firstNo) {
        firstNumber_input.sendKey(firstNo);
    };
    this.enterSecondNumber = function (secondNo) {
        secondNumber_input.sendKey(secondNo);
    };
    this.clickGo = function () {
        goButton.click();
    };

    this.verifyResult = function (result) {
        let output = element(by.cssContainingText('.ng-binding', result));
        expect(output.getText()).toEqual(result);
    }
};

module.exports = new homepage();
```
- tests/calculator.pom-spec.ts
```
let homepage = require('../pages/homepage');

describe('demo calculator tests', () => {
    const url = 'http://juliemr.github.io/protractor-demo/';

    beforeAll(() => {
        homepage.get(url);
    });

    it('should goto correct website', () => {
        expect(browser.getCurrentUrl()).toBe(url);
    });

    it('should add', () => {
        homepage.enterFirstNumber('1');
        homepage.enterSecondNumber('2');
        homepage.clickGo();
        homepage.verifyResult('3');
        browser.sleep(2000);
        // expect(input.getAttribute('value')).toBe('Foo123');
    });

});

```
- `npm install @types/node --save-dev`
