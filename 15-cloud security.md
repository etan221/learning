- following up with regarding security within the DB?: 1st: Kedar Krishnan, 2nd: Paul Wescott

Also, regarding Grouper stuff, the developer contacts Joe recommended were Wenlai Wang and Jessita Coutinho. The functional analyst for that team is Dan Erven.


Hi Kedar, my name is Eric. I am the developer of a new project: Research Budget Simplification. 
Our team is designing access right for this financial application. At some points in the future, we will retrieve salary information from Peoplesoft. Sam L recommended us to seek backend advice and security from you first.

Our architect, Nicholas and Andy, suggest us to seek advice from Paul Wescott regarding security within the DB?   


- [Salary Access (DRAFT)](https://wiki.auckland.ac.nz/pages/viewpage.action?pageId=184782217)
- [Access Control](https://wiki.auckland.ac.nz/pages/viewpage.action?pageId=183375461)

Youtube on 
### [AWS re:Inforce 2019: The Fundamentals of AWS Cloud Security (FND209-R](https://www.youtube.com/watch?v=-ObImxw1PmI)
- Fundamental patterns
    - how to recognize them, what to look for
- Permissions managmenet: IAM (identity and access management), control your cloud infrastructure
- Data Encryption: KMS (key management service), control your data
- Network security controls: VPC (virtual private cloud), control your network, use Kong instead of Kafka

- what need to know in order to secure cloud environment with them 

#### [IAM](https://youtu.be/-ObImxw1PmI?t=178)

[AWS Security Basics - AWS KMS, Client/Server Side Encryption, CMK, Data Key, Real World Use | Demo](https://www.youtube.com/watch?v=SOnJyqwGn1I)

uoa\etan221
eric.tan@auckland.ac.nz

draft list of gl code

at some point, retrieve salary: tranmission and storage securely
access right: groupers: 
and privacy

delotte - annel - guild/ new skills /

- invite specialist if it's good to discuss
- standup, is a dedicated time everyday for everyone in the team to discuss issues around the project. We should try best to keep it short and focus.
- I will discuss with them indiviidully, if its' purely technical. and will invite them to standup, if its about task priority, dependencies, arrangement, better to discuss in groups.
  
--------------------
    200915 cloud security on sensitive data 
--------------------
peoplesoft expose employee detail via api

frontend:
 - your frontend should not access peoplesoft api straight away. It should access your backend api, which is protected by oauth

backend: 
 - then your lambda, which is also secured. 
 - can decide which to go for those information, api or db.
 - for api, you can access, via Kong which is our on-prem api gateway, to call peoplesoft api to get employee/identity details, do some backend processing e.g. summing-up, then storing totol, and displaying data on screen.
 - for db, then just retrieve data from db, data can be stored in encrypted format, for 1 more level of security

people:
 - for peoplesoft ask Alumelu; for Kong, ask Jane Li. or you can find API via API Explorer.
 - currently there are api for retrieving employee detail, but none for salary. peoplesoft developer will need to create one for this application.   
 - Digital service is now automating a new Kong on cloud. so when the application is ready, you should call the Kong on-cloud instead.
 
aws account
 - currently, all developers share the same devops account, you should setup your own account, for editing data and calling apis 
 - ask cloud team to create a separate account for you to access dynamodb

environment: 
 - you can use sandbox for playing around, and trying new things, but you should deploy to nonprod for testing and accessing peoplesoft api there
 
privacy  
 - once requirement is finalised, check with privacy officer, what kind of information have to be store securely
 - we have a privacy officer dedicated to the university
 
options: depend on what information you want to store 
 - for individual's salary, you can store employeeId in the dynamodb, then look up peoplesoft api everytime, do calculation e.g. sum-up, then store the total.
 - for salary range, maybe you can save a copy in the application
 

peoplesoft - not for existing contract, new project, 4 people, salary range, 
obtain - expose api, secure, kong, api gateway, authorizate, oauth, backend call peoplesoft

api, secure, kong api gateway, authorization, oauth, backend call peoplesoft 
 
 
