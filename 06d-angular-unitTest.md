## [Angular Unit Testing](https://www.youtube.com/watch?v=BumgayeUC08)
    - Jasmine
    - Karma
    
### JavaScript Testing Introduction Tutorial - Unit Tests, Integration Tests & e2e Tests [video](https://www.youtube.com/watch?v=r9HdJ8P6GQI), [text](https://academind.com/learn/javascript/javascript-testing-introduction/), [source](https://github.com/academind/js-testing-introduction/tree/starting-setup)

- what is testing
    <your code> 
    -> expected result 
    -> test (automated & simplify) 
    -> success
    -> fail -> modify and fix <your code>
- [why test?](https://youtu.be/r9HdJ8P6GQI?t=162)
 - get an error immediately if you break code
 - save time
 - think about possible issues & bugs
 - integrate into build workflow
 - break up complex dependencies
 - improve your code
 - speeds up development because you don’t have to test everything manually after every change.
 - less error-prone
 - forced to think about your app and potential issues harder.
 
 - automated code, code that test code
 
- [different kinds of tests](https://youtu.be/r9HdJ8P6GQI?t=299)
 - fully isolated (e.g. testing one function) - unit tests - thousands of these!
 - with dependencies (e.g. testing a function that calls a function) - integration tests - good couples of these
 - full flow (e.g. validating the DOM after a click) - end-to-end tests - write a few of these
 - complexity grows, frequency decreases
 

- [Unit Testing Angular with Jasmine & Karma tutorial | Chapter 1 - Introduction](https://www.youtube.com/watch?v=wrLicPUsfTc&list=PL7uSdb_U7Fu8XlgXvBUOXyOE2Z_1B-jt9)

### UOA: Introduction to [front-end unit testing](https://wiki.auckland.ac.nz/display/AD/Meeting+Schedule), Harrison

- [Presentation slides](https://www.beautiful.ai/player/-M-SFWlqmFKtOV7p21kF/Introduction-to-Front-end-Unit-Testing-Presentation)
    [Angular service test code](https://bitbucket.org/uoa/angular-exemplar/src/master/src/app/service-example.service.spec.ts), on [bitbucket repo](https://bitbucket.org/uoa/angular-exemplar/src/master/)
    Jasmine - standalone project example: [jasmine-standalone-3.5.0.zip](https://wiki.auckland.ac.nz/download/attachments/162071516/jasmine-standalone-3.5.0.zip?version=1&modificationDate=1581038434000&api=v2)
    Jasmine - Node.js project example: [jasmine-nodejs.zip](https://wiki.auckland.ac.nz/download/attachments/162071516/jasmine-nodejs.zip?version=1&modificationDate=1581038429000&api=v2)
    Karma project example: [karma-nodejs.zip](https://wiki.auckland.ac.nz/download/attachments/162071516/karma-nodejs.zip?version=1&modificationDate=1581038436000&api=v2)

### Karma in [UOA deployment process](https://wiki.auckland.ac.nz/display/WT/AEM+Deployment)

### ResearchHub on [github](https://github.com/uoa-eresearch/hub-stack), Sam Kavanagh
- This is the ResearchHub monorepo, it has 3 different projects with different types of tests:
    - [research-hub-web](https://github.com/UoA-eResearch/hub-stack/tree/master/research-hub-web)
        - Unit tests with Jasmine
        - e2e tests with protractor (these can be run with either a local selenium instance, with BrowserStack automation on multiple browsers) as well as against either: a locally running instance or a remote instance
    - [cer-graphql](https://github.com/UoA-eResearch/hub-stack/tree/master/cer-graphql)
        - Contains integration tests with Jest
    - [serverless-now](https://github.com/UoA-eResearch/hub-stack/tree/master/serverless-now)
        - Contains integration tests using the Serverless Framework
- It also contains our Jenkinsfile which runs these tests depending on which project was updated (and runs our e2e tests on BrowserStack once the project has been redeployed)
- Andy: There is the Research Hub stuff that CeR have been working on: different testing strategies for different things. the Angular stuff has end-to-end and unit tests

### Angular Common Controls on [bitbucket](https://bitbucket.org/uoa/angular-common-controls/src/master/), Ryan
- try to use the default templated angular tools where they make sense eg. jest
- maybe you can see this one which covers a hard to test situation: 
- the tests are component based so are in the same folders as the components, you can just run `npm test` after `npm install` to run the tests

- general: because there are not so many of us front-end devs around, we don't have strict standards but instead try to sharpen each other and meet every couple of weeks to discuss the future of tech and what we are doing
- in my team: we have a tester who does integration and end-to-end. but he expects us to do unit tests


### [Udemy Angular](https://www.udemy.com/course/the-complete-angular-master-class/learn/lecture/7716668#overview)
 - Unit Test with Jasmine 
- error: `Cannot find module '@angular/http' or its corresponding type declarations`
- `npm install @angular/http@latest`
------------
- [Testing Setup for React](https://youtu.be/r9HdJ8P6GQI?t=570)
    - Test Runner
        - execute tests, and summarize results
        - e.g. Mocha
    - Assertion Library
        - define testing logic, conditions
        - e.g. Chai

    - e.g. Jest
    
    - Headless Browser
        - simulartes browser interaction
        - e.g. puppeteer
        
    (Mocha + Chai) or Jest  = Unit + Integration
    puppeteer = e2e
    


### Testing account in nonprod
