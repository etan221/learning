
- [Solution Design of Research Budget Simplification](https://wiki.auckland.ac.nz/display/UoASA/Solution+Design+Document+for+Research+Budget+Simplification)

#### AWS IAM
[Controlling access to AWS resources using policies](https://docs.aws.amazon.com/IAM/latest/UserGuide/access_controlling.html)

```
AWS then checks that you (the principal) are authenticated (signed in) and authorized (have permission) to perform the specified action on the specified resource. During authorization, AWS checks all the policies that apply to the context of your request. Most policies are stored in AWS as JSON documents and specify the permissions for principal entities. For more information about policy types and uses, see Policies and permissions in IAM.
```

#### [Identity-based policies](https://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies.html#policies_id-based)

- Identity-based policies can be further categorized:
    1. Managed policies – Standalone identity-based policies that you can attach to multiple users, groups, and roles in your AWS account. There are two types of managed policies:
        - AWS managed policies – Managed policies that are created and managed by AWS.
        - Customer managed policies – Managed policies that you create and manage in your AWS account. Customer managed policies provide more precise control over your policies than AWS managed policies.

    2. Inline policies – Policies that you add directly to a single user, group, or role. Inline policies maintain a strict one-to-one relationship between a policy and an identity. They are deleted when you delete the identity.


- [AWS IAM Tutorial | IAM Group, Policy, User, Role | Amazon Web Services Basics](https://www.youtube.com/watch?v=8WLoAfydS_4)