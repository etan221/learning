

### [CSS Grid Layout Crash Course](https://www.youtube.com/watch?v=jV8B24rSN5o)


### [Learn CSS in 20 Minutes](https://www.youtube.com/watch?v=1PnVor36_40) in series: [learn x in y mins](https://www.youtube.com/playlist?list=PLZlA0Gpn_vH85jM1TWO6TdCtSr6ruglWn)
### [Learn CSS Position In 9 Minutes](https://www.youtube.com/watch?v=jx5jmI0UlXU&list=PLZlA0Gpn_vH85jM1TWO6TdCtSr6ruglWn&index=2)
### [Learn Flexbox in 15 Minutes](https://www.youtube.com/watch?v=fYq5PXgSsbE)
### [Learn HTML Forms In 25 Minutes](https://www.youtube.com/watch?v=fNcJuPIZ2WE&list=PLZlA0Gpn_vH85jM1TWO6TdCtSr6ruglWn&index=9)
### [Learn CSS Media Query In 7 Minutes](https://www.youtube.com/watch?v=yU7jJ3NbPdA&list=PLZlA0Gpn_vH85jM1TWO6TdCtSr6ruglWn&index=11)
### [Learn CSS Grid in 20 Minutes](https://www.youtube.com/watch?v=9zBsdzdE4sM&list=PLZlA0Gpn_vH85jM1TWO6TdCtSr6ruglWn&index=13)
### [Learn CSS Box Model In 8 Minutes](https://www.youtube.com/watch?v=rIO5326FgPE&list=PLZlA0Gpn_vH85jM1TWO6TdCtSr6ruglWn&index=14)
### [Learn CSS Display Property In 4 Minutes](https://www.youtube.com/watch?v=Qf-wVa9y9V4&list=PLZlA0Gpn_vH85jM1TWO6TdCtSr6ruglWn&index=15)
### [Learn CSS Pseudo Elements In 8 Minutes](https://www.youtube.com/watch?v=OtBpgtqrjyo&list=PLZlA0Gpn_vH85jM1TWO6TdCtSr6ruglWn&index=20)
### [Learn Every CSS Selector In 20 Minutes](https://www.youtube.com/watch?v=l1mER1bV0N0&list=PLZlA0Gpn_vH85jM1TWO6TdCtSr6ruglWn&index=23)
### [Learn CSS Calc In 6 Minutes](https://www.youtube.com/watch?v=x7EWFoRzAkk&list=PLZlA0Gpn_vH85jM1TWO6TdCtSr6ruglWn&index=25)
### [Learn CSS Specificity In 11 Minutes](https://www.youtube.com/watch?v=CHyPGSpIhSs&list=PLZlA0Gpn_vH85jM1TWO6TdCtSr6ruglWn&index=34)

 - flexbox - one dimension
 - css grids - two dimension, columns and rows

- [word-break property](https://www.youtube.com/watch?v=dRGi2iBuOIo)
- [wrap text with CSS](https://www.youtube.com/watch?v=5VP-vJDyoWU)