## REST API

[AWS Lambda & DynamoDB Rest API tutorial (full CRUD)](https://www.youtube.com/watch?v=Pa99PT16tmw)

### AWS Lambda & DynamoDB Rest API tutorial (full CRUD)

- DynanoDB
    - Document Based (NoSQL)
    - Read heavy oriented
    - High availablility
    - SSD (solid state drive) Storage
    - low latency
    - full customized

`cmd`

`git bash`

`cd /c/opt/uoa_project/`

`mkdir sls-api`

`cd sls-api`

`sls create -t aws-nodejs`

`code .`

`npm install uuid`

***
beware of tab and space. This bug was fixed by tab properly.

`sls deploy`
```
may be error:  Missing credentials in config, if using AWS_CONFIG_FILE, set AWS_SDK_LOAD_CONFIG=1
```

- handler.js
```js
'use strict';
const AWS = require('aws-sdk');
const db = new AWS.DynamoDB.DocumentClient({apiVersion: '2012-08-10'});
//const uuid = require('uuid/v4');
//const {"v4": uuidv4} = require('uuid');

const postsTable = process.env.POSTS_TABLE;

console.log("postsTable: ", postsTable);

function response(statusCode, message){
  console.log("response.statusCode:", statusCode);
  console.log("response.message:", message);

  return {
    statusCode: statusCode,
    body: JSON.stringify(message)
  };
}

function sortByDate(a,b){
  if (a.createdAt > b.createdAt) {
    return -1;
  } 
  return 1;  
}

module.exports.createPost = (event, context, callback) => {
  const requestBody = JSON.parse(event.body);

  if (!requestBody.title || requestBody.title.trim() === '' 
  || !requestBody.body || requestBody.body.trim() === '' ){
    return callback(null, response(400, {error: 'Post must have a title and a body and they must not be empty.'}));
  }

  const post = {
    //id: uuid(),
    id: Date.now().toString(),
    // id: Date.now() + "A",
    createdAt: new Date().toISOString(),
    userId: 1,
    title: requestBody.title,
    body: requestBody.body
  };

  console.log("post:",post);

  return db.put({
    TableName: postsTable,
    Item: post
  }).promise().then(()=>{
    callback(null, response(201, post));
  })
  .catch(err => response(null, response(err.statusCode, err)));
};

module.exports.getAllPosts = (event, context, callback) => {
  return db.scan({
    TableName: postsTable
  }).promise().then(res => {
    console.info("res:", res);
    callback(null, response(200, res.Items.sort(sortByDate)));
  })
  .catch(err => callback(null, response(err.statusCode, err)));
};

module.exports.getPosts = (event, context, callback) => {
  const numberOfPosts = event.pathParameters.number;
  const params = {
    TableName: postsTable,
    Limit: numberOfPosts
  }
  return db.scan(params)
    .promise()
    .then(res => {
      console.info("res:", res);
      callback(null, response(200, res.Items.sort(sortByDate)));
    }).catch(err => callback(null, response(err.statusCode, err)));
};

module.exports.getPost = (event, context, callback) => {
  const id = event.pathParameters.id;
  console.info("id: ", id);
  const params = {
    Key: {
      id: id
    },
    TableName: postsTable
  }
  return db.get(params)
    .promise()
    .then(res => {
      console.log("res:", res);
      if (res.Item) callback(null, response(200, res.Item));
      else callback(null, response(404, {error: 'Post not found'}));
    }).catch(err => callback(null, response(err.statusCode, err)));
};

module.exports.updatePost = (event, context, callback) => {
  const id = event.pathParameters.id;
  const body = JSON.parse(event.body);
  const paramName = body.paramName;
  const paramValue = body.paramValue;

  const params = {
    Key: {
      id: id
    },
    TableName: postsTable,
    ConditionExpression: 'attribute_exists(id)',
    UpdateExpression: 'set ' + paramName + ' = :v',
    ExpressionAttributeValues: {
      ':v': paramValue
    },
    ReturnValue: 'ALL_NEW'
  };

  return db.update(params)
    .promise()
    .then(res => {
      console.info("res:", res);
      callback(null, response(200, res.Attributes));
    })
    .catch(err => callback(null, response(err.statusCode, err)));
};

module.exports.deletePost = (event, context, callback) => {
  const id = event.pathParameters.id;
  const params = {
    Key: {
      id: id
    },
    TableName: postsTable
  };
 
  return db.delete(params)
    .promise()
    .then(() => callback(null, response(200, {message: 'Post deleted successfully'})))
    .catch(err => callback(null, response(err.statusCode, err)));
}

```
- serverless.yml
```yml
service: sls

custom:
  settings:
    POSTS_TABLE: posts

provider:
  name: aws
  runtime: nodejs12.x
  #environments: ${self:custom:settings}
  environment:
    POSTS_TABLE: posts
  region: ap-southeast-2
  

  iamRoleStatements: 
    - Effect: "Allow"
      Action: 
        - dynamodb:DescribeTable
        - dynamodb:Scan
        - dynamodb:GetItem
        - dynamodb:PutItem
        - dynamodb:UpdateItem
        - dynamodb:DeleteItem
      Resource:
        - "arn:aws:dynamodb:${self:provider.region}:*:table/${self:custom.settings.POSTS_TABLE}"


functions:
  createPost:
    handler: handler.createPost
    # environment:
    #     POSTS_TABLE: posts
    events:
    - http:
        path: /post
        method: post
  getAllPosts:
    handler: handler.getAllPosts
    events:
    - http:
        path: /posts
        method: get
  getPosts:
    handler: handler.getPosts
    events:
    - http:
        path: /posts/{number}
        method: get
  getPost:
    handler: handler.getPost
    events:
    - http:
        path: /post/{id}
        method: get
  updatePost:
    handler: handler.updatePost
    events:
    - http:
        path: /post/{id}
        method: put
  deletePost:
    handler: handler.deletePost
    events:
    - http:
        path: /post/{id}
        method: delete

resources:
  Resources:
    PostsTable:
      Type: AWS::DynamoDB::Table
      Properties:
        AttributeDefinitions:
        - AttributeName: "id"
          AttributeType: "S"
        KeySchema:
        - AttributeName: "id"
          KeyType: "HASH"
        ProvisionedThroughput:
          ReadCapacityUnits: 1
          WriteCapacityUnits: 1
        TableName: ${self:custom.settings.POSTS_TABLE}
```

`sls deploy`

```
endpoints:
  POST - https://tol21npri3.execute-api.ap-southeast-2.amazonaws.com/dev/post
  GET - https://tol21npri3.execute-api.ap-southeast-2.amazonaws.com/dev/posts
  GET - https://tol21npri3.execute-api.ap-southeast-2.amazonaws.com/dev/posts/{number}
  GET - https://tol21npri3.execute-api.ap-southeast-2.amazonaws.com/dev/post/{id}
  PUT - https://tol21npri3.execute-api.ap-southeast-2.amazonaws.com/dev/post/{id}
  DELETE - https://tol21npri3.execute-api.ap-southeast-2.amazonaws.com/dev/post/{id}
```

`cd C:\opt\uoa_project\utility-aws-cli-access`

`python aws_saml_login.py --idp iam.auckland.ac.nz`

`cd C:\opt\uoa_project\sls-api`

`sls deploy --aws-profile uoa`

```
Serverless: Stack update finished...
Service Information
service: sls
stage: dev
region: ap-southeast-2
stack: sls-dev
resources: 40
api keys:
  None
endpoints:
  POST - https://iddshnhd26.execute-api.ap-southeast-2.amazonaws.com/dev/post
  GET - https://iddshnhd26.execute-api.ap-southeast-2.amazonaws.com/dev/posts
  GET - https://iddshnhd26.execute-api.ap-southeast-2.amazonaws.com/dev/posts/{number}
  GET - https://iddshnhd26.execute-api.ap-southeast-2.amazonaws.com/dev/post/{id}
  PUT - https://iddshnhd26.execute-api.ap-southeast-2.amazonaws.com/dev/post/{id}
  DELETE - https://iddshnhd26.execute-api.ap-southeast-2.amazonaws.com/dev/post/{id}
functions:
  createPost: sls-dev-createPost
  getAllPosts: sls-dev-getAllPosts
  getPosts: sls-dev-getPosts
  getPost: sls-dev-getPost
  updatePost: sls-dev-updatePost
  deletePost: sls-dev-deletePost
layers:
  None
```
- Testing: Postman

`POST https://tol21npri3.execute-api.ap-southeast-2.amazonaws.com/dev/post`
```
{
	"title": "Post One",
	"body": "This is the first post"
}
```
`PUT https://tol21npri3.execute-api.ap-southeast-2.amazonaws.com/dev/post/1596063004171`
```
{
	"paramName": "title",
	"paramValue": "The second post"
}
```

### clone from Repository, instead of create locally

`git clone git@bitbucket.org:etan221/backend-demo.git`

