##
- architecture
- [Todo App with AWS Lambda, Angular, Mongo (LAM stack)](https://www.youtube.com/watch?v=K3gShFhIuKU) 2018

- Angular UI
- [Angular 8 + Spring Boot CRUD Example Tutorial](https://www.youtube.com/watch?v=lYMKywB46go) 2019

- Angular UI
- [Angular 8 Tutorial: Learn to Build Angular 8 CRUD Web App + Source Code](https://www.youtube.com/watch?v=Ak139ZlTutE) 2019
- [text](https://www.djamware.com/post/5d0eda6f80aca754f7a9d1f5/angular-8-tutorial-learn-to-build-angular-8-crud-web-app)

- with AWS  
- [Building an ecommerce webapp with AWS and Angular](https://www.youtube.com/playlist?list=PLvmxnsCyoh64kn_H-7OJGubNMQhJzOmbh)