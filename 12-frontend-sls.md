## frontend development and serverless

### [1 - Serverless Frontend](https://www.youtube.com/watch?v=CawN9GCqaeQ&list=PLIIjEI2fYC-DX8ozrwHkz81muM7kmbC3F)

### [2 - Different Options for Building Serverless Frontends](https://www.youtube.com/watch?v=I2myQXtAdlU&list=PLIIjEI2fYC-DX8ozrwHkz81muM7kmbC3F&index=2)

#### Tools to create serverless frontends
- Popular frontend frameworks: React, Next.js, Gatsby, Vue, Nuxt
- Popular static site generators: Jekyll, Hugo, Pelican
- Infrastructure Resources: Amazon S3 website hosting / Netlify, GitHub Pages, Surge.sh / AWS Lambda or Lambda at Edge
- Serverless Framework Tools: Serverless Components, Serverless Plugins

### [3 - Using Serverless Components to Deploy Frontend Applications](https://www.youtube.com/watch?v=dVz6kv0YhBk&list=PLIIjEI2fYC-DX8ozrwHkz81muM7kmbC3F&index=3)

Course Structure
- Deploying websites with Serverless Components

[The state of serverless components](https://youtu.be/dVz6kv0YhBk?list=PLIIjEI2fYC-DX8ozrwHkz81muM7kmbC3F&t=67): 
- HTTPS
- CloudFront CDN
- Custom Domain Configuration
- Lambda@Edge

### [8 - Route 53 to Register a Custom Domain](https://www.youtube.com/watch?v=8UdD-dmflR0&list=PLIIjEI2fYC-DX8ozrwHkz81muM7kmbC3F&index=8)

Manually configuring SSL, a CDN, and a custom domain
- registering a cusomt domain
- requesting an SSL certiciate
- creating and configuring a cloudfront distribution
- redeploying our site and invaliding the cache

- [Route53](https://youtu.be/8UdD-dmflR0?list=PLIIjEI2fYC-DX8ozrwHkz81muM7kmbC3F&t=84)

### [9 - Using the Amazon Certificate Manager to Issue an SSL Certification](https://www.youtube.com/watch?v=Ge-dkZgqLKg&list=PLIIjEI2fYC-DX8ozrwHkz81muM7kmbC3F&index=9)

