## Angular Module and Components

vscode: [shortcut img](https://code.visualstudio.com/assets/docs/getstarted/tips-and-tricks/KeyboardReferenceSheet.png)

### run powershell as administrator
- vscode: [switch between command prompt vs powershell](https://www.youtube.com/watch?v=6bzmdUfZZ-w)
```
PS C:\WINDOWS\system32> set-executionpolicy remotesigned

Execution Policy Change
The execution policy helps protect you from scripts that you do not trust. Changing the execution policy might expose
you to the security risks described in the about_Execution_Policies help topic at
https:/go.microsoft.com/fwlink/?LinkID=135170. Do you want to change the execution policy?
[Y] Yes  [A] Yes to All  [N] No  [L] No to All  [S] Suspend  [?] Help (default is "N"): A
PS C:\WINDOWS\system32>
```

- vscode `Ctrl + Shife + '`
### create article module
`ng g m article`

```
CREATE src/app/article/article.module.ts (193 bytes) 
```
- article.module.ts
```
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class ArticleModule { }
```
- add ArticleModule to app.module.ts 
```
import { ArticleModule } from './article/article.module';
...

@NgModule({
  declarations: [
    ...
  ],
  imports: [
    ...,
    ArticleModule
  ],
  ...
})
export class AppModule { }
```

- create article.module and update app.module

`ng g m article -m app`

```
CREATE src/app/article/article.module.ts (193 bytes)
UPDATE src/app/app.module.ts (775 bytes)
```
### create article-list componenet

`cd src/app/article`

```
C:\opt\uoa_project\demo\src\app\article>
```

`ng g c article-list`

```
CREATE src/app/article/article-list/article-list.component.html (27 bytes)
CREATE src/app/article/article-list/article-list.component.spec.ts (664 bytes)
CREATE src/app/article/article-list/article-list.component.ts (298 bytes)
CREATE src/app/article/article-list/article-list.component.css (0 bytes)
UPDATE src/app/article/article.module.ts (291 bytes)
```

- article.module.ts, exports ArticleListComponent so that app.component.html could use <app-article-list>.
```
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArticleListComponent } from './article-list/article-list.component';

@NgModule({
  declarations: [ArticleListComponent],
  imports: [
    CommonModule
  ],
  exports: [ArticleListComponent]
})
export class ArticleModule { }
```

- create component in article module
- `cd C:\opt\uoa_project\demo\src\app\article`

`ng g c article-header`

```
CREATE src/app/article/article-header/article-header.component.html (29 bytes)
CREATE src/app/article/article-header/article-header.component.spec.ts (678 bytes)
CREATE src/app/article/article-header/article-header.component.ts (306 bytes)
CREATE src/app/article/article-header/article-header.component.css (0 bytes)
UPDATE src/app/article/article.module.ts (434 bytes)
```

`ng g c article-body`

```
CREATE src/app/article/article-body/article-body.component.html (27 bytes)
CREATE src/app/article/article-body/article-body.component.spec.ts (664 bytes)
CREATE src/app/article/article-body/article-body.component.ts (298 bytes)
CREATE src/app/article/article-body/article-body.component.css (0 bytes)
UPDATE src/app/article/article.module.ts (534 bytes)
```

- article.module.ts
```
@NgModule({
  declarations: [ArticleListComponent, ArticleHeaderComponent, ArticleBodyComponent],
  imports: 
  exports: [ArticleListComponent]   // exports components that only used by callers, header/body are not referenced, so don't need to export. 
  ...
})
```
- need to pass variable (article) to components
- 
- in article-header.component.ts, define a variable: article, that can accept passing-in values.
```
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-article-header',
    ...
})
export class ArticleHeaderComponent implements OnInit {
  @Input()
  article;
}
```

- in article-list.component.html, pass the value to article-header
```
<article class="post" id="post{{idx}}" *ngFor="let article of articles | slice:0:3; let idx=index">
  <app-article-header [article]=article></app-article-header>
  <app-article-body [article]=article></app-article-body>
</article>
```

### vscode: keyboard shortcuts For productivity, [video](https://www.youtube.com/watch?v=Xa5EU-qAv-I), [text](https://gist.github.com/bradtraversy/b28a0a361880141af928ada800a671d9)

`Ctrl+Shift+L` selects all occurrences of the highlighted text, very handy to rename a method, a variable or refactor a duplicated bit of code at all places at once

[ui component](https://ionicframework.com/docs/components)

### vscode: [install Git inside of VSCode](https://www.youtube.com/watch?v=F2DBSH2VoHQ)

`git status`

`git add readme.md`

`git log`

`git remote add origin https://bitbucket.org/etan221/demo.git`

`git push -u origin master`

## Life-cycle of Angular Component 

```
import { Component, OnInit, Input } from '@angular/core';
@Component({
  selector: 'app-article-body',
  templateUrl: './article-body.component.html',
  styleUrls: ['./article-body.component.css']
})
export class ArticleBodyComponent implements OnInit {

  @Input()
  article;

  constructor() { }

  ngOnInit(): void {
  }
}
```
1. constructor()
- use dependency injection (DI)

2. ngOnInit()
- set default value to variables
- request data to backend, assign response value to variables

3. ngOnDestroy()
- less use, as garbage collected by memory 
- when use rxjs, unsubscribe in ngOnDestroy
- goto `implements OnInit`, add `, OnDestroy` tab 
- example: article-body.component.ts
```
import { Component, OnInit, Input, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-article-body',
  templateUrl: './article-body.component.html',
  styleUrls: ['./article-body.component.css']
})
export class ArticleBodyComponent implements OnInit, OnDestroy {

  @Input()
  article;

  constructor() { }
  
  ngOnInit(): void {
  }
  
  ngOnDestroy(): void {
    throw new Error("Method not implemented.");
  }
}

```

- example: article-list.component.ts
```
export class ArticleListComponent implements OnInit {

  articles;

  constructor() { }

  ngOnInit(): void {
    this.articles = [
      {
        "id": 1,
        "href": "http://blog.miniasp.com/post/2016/04/30/Visual-Studio-Code-from-Command-Prompt-notes.aspx",
        "subject": {
          "title": "從命令提示字元中開啟 Visual Studio Code 如何避免顯示惱人的偵錯訊息",
          "subtitle": "test"
        },
        "date": "2016/04/30 18:05",
        "author": "Will 保哥",
        "category": "Visual Studio",
        "category-link": "http://blog.miniasp.com/category/Visual-Studio.aspx",
        "summary": "<p>由於我的 Visual Studio Code 大部分時候都是在命令提示字元下啟動，所以只要用 <strong><font color='#ff0000' face='Consolas'>code .</font></strong>就可以快速啟動 Visual Studio Code 並自動開啟目前所在資料夾。不過不知道從哪個版本開始，我在啟動 Visual Studio Code 之後，卻開始在原本所在的命令提示字元視窗中出現一堆惱人的偵錯訊息，本篇文章試圖解析這個現象，並提出解決辦法。</p><p>... <a class='more' href='http://blog.miniasp.com/post/2016/04/30/Visual-Studio-Code-from-Command-Prompt-notes.aspx#continue'>繼續閱讀</a>...</p>"
      },
```

- Open closed principle: `open for extension, but closed for modification`

### father-son communication
- father pass value to child via property binding `[article]="article"` 
- child emits an event to father `(delete)="doDelete($event)"`
- `<app-article-header [article]="article" (delete)="doDelete($event)"></app-article-header>`
- article-header.component.ts: ready
```
import { ... , Output, EventEmitter } from '@angular/core';

@Component({
    ...
})
export class ArticleHeaderComponent implements OnInit {

  @Input()
  article;

  @Output()
  delete = new EventEmitter<any>();

  deleteArticle(){
    this.delete.emit(this.article);
  }
    ...
}
```

- article-header.component.html: emit
-
```
    <span>
      <button (click)="deleteArticle">Delete article</button>
    </span>
```

### @Output

v0.1 - [finished: data binding](https://www.udemy.com/course/angular-zero/learn/lecture/10011574#questions)
v0.2 - [finsihed: module compoenents](https://www.udemy.com/course/angular-zero/learn/lecture/10040706#questions)
v0.4 - [finsihed: ngOnChange](https://www.udemy.com/course/angular-zero/learn/lecture/10040706#questions)
v0.5 - [finsihed: articleService](https://www.udemy.com/course/angular-zero/learn/lecture/10053356#questions)
v0.6 - [finsihed: HttpClient](https://www.udemy.com/course/angular-zero/learn/lecture/10053362#questions/6335242)

### deploy to s3
`git checkout tags/<tag_name>`
`ng build --prod`
`cd dist\demo1`
### 

- article-header.componet.ts

`isEdit = false;`


1. Two-way binding

- article-header.componet.html
`[(ngModel)]="article.title"`

```html
<a [href]="article.href">{{ article.title }}</a>
<input type="text" size="70" [(ngModel)]="article.title">
```

2. son edit father data

- article-header.componet.html
```html
<a [href]="article.href">{{ article.title }}</a>
<input type="text" size="70" [value]="article.title"
  (keyup.enter)="article.title = $event.target.value"
  (keyup.escape)="$event.target.value = article.title">
```

3. immutable

- article-header.componet.ts

`newTitle = '';`


- article-header.componet.html
```html
<a [href]="article.href">{{ article.title }}</a>
<input type="text" size="70" [value]="newTitle"
  (keyup.enter)="newTitle=$event.target.value"
  (keyup.escape)="$event.target.value=newTitle">
```

### life cycle: [ngOnChanges](https://www.udemy.com/course/angular-zero/learn/lecture/10040718#questions)

- article-body.component.ts
```js
import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-article-body',
  templateUrl: './article-body.component.html',
  styleUrls: ['./article-body.component.css']
})
export class ArticleBodyComponent implements OnInit, OnChanges {

  @Input()
  article;

  @Input()
  counter;

  constructor() {
    console.log("BodyComponent: constructor");
  }

  ngOnChanges(changes: SimpleChanges): void {  ngOnInit(): void {

    setTimeout(() => {
      this.counter++;
    }, 5000);
    console.log("BodyComponent " + this.article.id + ": ngOnChanges", changes);
    // throw new Error("Method not implemented.");
  }

  ngOnInit(): void {
    console.log("BodyComponent " + this.article.id + ": ngOnInit");
  }
}
```

- article-list.component.ts, timeout to trigger
```
  ngOnInit(): void {

    setTimeout(() => {
      this.counter++;
    }, 5000);
```
- ngFor 2 articles, thus 2 constructors
- constructor -> ngOnChanges -> ngOnInit
- what is ngOnChanges?
- when will it be invoked? ans: when father pass value to son via @Input
```
2 times: ArticleBodyComponent: constructor
BodyComponent: constructor
article-body.component.ts:21 BodyComponent 1: ngOnChanges {article: SimpleChange, counter: SimpleChange}
article-body.component.ts:26 BodyComponent 1: ngOnInit
article-body.component.ts:21 BodyComponent 2: ngOnChanges {article: SimpleChange, counter: SimpleChange}
article-body.component.ts:26 BodyComponent 2: ngOnInit
core.js:26543 Angular is running in development mode. Call enableProdMode() to enable production mode.
client:52 [WDS] Live Reloading enabled.
article-body.component.ts:21 BodyComponent 1: ngOnChanges {counter: SimpleChange}counter: SimpleChangecurrentValue: 1firstChange: falsepreviousValue: 0__proto__: Object__proto__: Object
article-body.component.ts:21 BodyComponent 2: ngOnChanges {counter: SimpleChange}
```

```
counter: SimpleChange {
    currentValue: 1, 
    firstChange: false,
    previousValue: 0
}
```

- article-list.component.ts
```html
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-article-list',
  templateUrl: './article-list.component.html',
  styleUrls: ['./article-list.component.css']
})
export class ArticleListComponent implements OnInit {

  articles: Array<any>;

  counter = 0;

  constructor() { }

  deleteArticle(inArticle): void{
    this.articles = this.articles.filter((article) => { // filter: false: take away, true: keep
      return article.id !== inArticle.id;   // no eqauls() in javascript, compare .id instead
    })
  }

  changeArticleTitle($event: any): void{
    this.articles = this.articles.map((article) => {    // Array.map return a new Array
      if (article.id === $event.id){
        return Object.assign({}, article, $event);      // return new article object, put old one first then put new one
      }
      return article;
    })
    // console.log("article-list:changeTitle():" + $event.id, $event.title);
  }

  ngOnInit(): void {

    setTimeout(() => {  // trigger ngOnChange in child component, article-body subscribed on "counter"
      this.counter++;
    }, 5000);

    this.articles = [
      {
        id: 1,
        href: 'http://blog.miniasp.com/post/2016/04/30/Visual-Studio-Code-from-Command-Prompt-notes.aspx',
        title: '從命令提示字元中開啟 Visual Studio Code 如何避免顯示惱人的偵錯訊息',
        date: '2016/04/30 18:05',
        author: 'Will 保哥',
        category: 'Visual Studio',
        'category-link': 'http://blog.miniasp.com/category/Visual-Studio.aspx',
        summary: '<p>由於我的 Visual Studio Code 大部分時候都是在命令提示字元下啟動，所以只要用 <strong><font color=\'#ff0000\' face=\'Consolas\'>code .</font></strong>就可以快速啟動 Visual Studio Code 並自動開啟目前所在資料夾。不過不知道從哪個版本開始，我在啟動 Visual Studio Code 之後，卻開始在原本所在的命令提示字元視窗中出現一堆惱人的偵錯訊息，本篇文章試圖解析這個現象，並提出解決辦法。</p><p>... <a class=\'more\' href=\'http://blog.miniasp.com/post/2016/04/30/Visual-Studio-Code-from-Command-Prompt-notes.aspx#continue\'>繼續閱讀</a>...</p>'
      },
      ...
    ];
  }
}
```

- article-list.component.html
```
<article class="post" id="post{{idx}}" *ngFor="let article of articles | slice:0:2; let idx=index">
  <app-article-header [article]="article" (delete)="deleteArticle($event)" (changeTitle)="changeArticleTitle($event)"></app-article-header>
  <app-article-body [article]="article" [counter]="counter"></app-article-body>
</article>
```

- article-header.component.ts
```
import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-article-header',
  templateUrl: './article-header.component.html',
  styleUrls: ['./article-header.component.css']
})
export class ArticleHeaderComponent implements OnInit, OnChanges {

  @Input()
  article;

  original_article;

  @Output()
  delete = new EventEmitter<any>();

  @Output()
  changeTitle = new EventEmitter<any>();

  isEdit = false;
  newTitle = '';

  constructor() {
  }

  ngOnChanges(changes: SimpleChanges): void { // essential
    if (changes.article){
      this.original_article = changes.article.currentValue;   // save passed-in value for undo
      this.article = Object.assign({}, changes.article.currentValue); // create new object in ArticleHeader for two-way binding: ngModel
    }
  }

  ngOnInit(): void {
  }

  onDeleteArticle(): void{
    this.delete.emit(this.article); // trigger father: article-list
  }

  doEditTitle(title): void{
    this.changeTitle.emit(this.article);  // trigger father: article-list
  }

  undoEditTitle(): void{
    this.article = Object.assign({}, this.original_article);  // undo edit title
    this.isEdit = false;
  }
}
```

- article-header.component.html
```
<header class="post-header">
  <h2 class="post-title">
    <a *ngIf="!isEdit" [href]="article.href">{{ article.title }}</a>
    <input *ngIf="isEdit" type="text" size="70" [(ngModel)]="article.title"
      (keyup.enter)="doEditTitle()"
      (keyup.escape)="undoEditTitle()">
  </h2>
  <div class="post-info clearfix">
    <span class="post-date"><i class="glyphicon glyphicon-calendar"></i>{{ article.date | date:"yyyy-MM-dd" }}</span>
    <span class="post-author"><i class="glyphicon glyphicon-user"></i>
      <a href="http://blog.miniasp.com/author/will.aspx">{{ article.author }}</a></span>
    <span class="post-category"><i class="glyphicon glyphicon-folder-close"></i>
      <a [href]="article['category-link']">{{ article.category }}</a>
    </span>
    <span>
      <button (click)="onDeleteArticle()">Delete Article</button>
      <button *ngIf="!isEdit" (click)="isEdit=true">Edit Article</button>
      <button *ngIf="isEdit" (click)="undoEditTitle()">Undo Edit</button>
    </span>
  </div>
</header>
```


### Service Component
- is a class
- contain attributes and methods
- externalise data attributes and related operations to service component
- for sharing data and logic

`cd C:\opt\uoa_project\demo`

`ng g s article`

```
CREATE src/app/article.service.spec.ts (362 bytes)
CREATE src/app/article.service.ts (136 bytes)
```

- article.module.ts, `ArticleService`
```
import { ArticleService } from './../article.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArticleListComponent } from './article-list/article-list.component';
import { ArticleHeaderComponent } from './article-header/article-header.component';
import { ArticleBodyComponent } from './article-body/article-body.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule, FormsModule
  ],
  providers: [ArticleService],
  declarations: [ArticleListComponent, ArticleHeaderComponent, ArticleBodyComponent],
  exports: [ArticleListComponent]
})
export class ArticleModule { }
```

- article-list.component.ts
```
articleSvc: ArticleService;
constructor(articleSvc: ArticleService) { // inject service when construct
  this.articleSvc = articleSvc;
}
```
- equivalent to 
```
constructor(private articleSvc: ArticleService) {} // inject service when construct
```

```
ngOnInit(): void {
  this.articleSvc.run();
}
```

### vscode: plugin: `move ts`

- `shift + click` to choose 2 files: article.service.ts, article.service.spec.ts
- right click, choose `move typescript` to `..\src\app\article`
```
Moving:
           c:\opt\uoa_project\demo\src\app\article.service.ts -> c:\opt\uoa_project\demo\src\app\article\article.service.ts
           c:\opt\uoa_project\demo\src\app\article.service.spec.ts -> c:\opt\uoa_project\demo\src\app\article\article.service.spec.ts
--------------------------------------------------
Files changed:
c:\opt\uoa_project\demo\src\app\article\article-list\article-list.component.ts

```

### problem when dependency injection (DI)
- equal to  `Can't resolve all paramters for DataService: (?)` in Angular 6
```
core.js:561 Uncaught Error: Angular JIT compilation failed: '@angular/compiler' not loaded!
  - JIT compilation is discouraged for production use-cases! Consider AOT mode instead.
  - Did you bootstrap using '@angular/platform-browser-dynamic' or '@angular/platform-server'?
  - Alternatively provide the compiler with 'import "@angular/compiler";' before bootstrapping.
    at getCompilerFacade (core.js:561)
    at Function.get (core.js:25539)
    at registerNgModuleType (core.js:23869)
    at core.js:23880
    at Array.forEach (<anonymous>)
    at registerNgModuleType (core.js:23880)
    at new NgModuleFactory$1 (core.js:23977)
    at compileNgModuleFactory__POST_R3__ (core.js:27496)
    at PlatformRef.bootstrapModule (core.js:27734)
    at Module../src/main.ts (main.ts:11)
```


https://code.visualstudio.com/docs/getstarted/tips-and-tricks

https://bitbucket.org/uoa/ionic-shell-application/src/master/

https://styleguide.blogs.auckland.ac.nz/resources/