## Research budget

- `npm install`
- `ionic serve --port=8100` -> `localhost:8100`
- `npm start` or `ng serve` -> `localhost:4200` 

### A. Deploy to environment
#### sandbox
- `ng build --configuration=sandbox`
- `npm run sandbox-gencre`
- `npm run sandbox-deploy`
- or `ng build --configuration=sandbox & npm run sandbox-gencre & npm run sandbox-deploy`
   
#### nonprod
- `ng build --configuration=nonprod`
- `npm run nonprod-gencre` or `python C:\\opt\\uoa_project\\utility-aws-cli-access\\aws_saml_login.py --idp iam.auckland.ac.nz --account devops --profile uoa-nonprod`
- `npm run nonprod-deploy` or `aws s3 sync www/ s3://uoa-research-nonprod --delete --profile uoa-nonprod`
- or `ng build --configuration=nonprod & npm run nonprod-gencre & npm run nonprod-deploy`

### B. Cognito (sandbox)
- [SPA Openid-Oauth2.0 authentication and authorization with AWS Cognito](https://wiki.auckland.ac.nz/display/ITCB/SPA+Openid-Oauth2.0+authentication+and+authorization+with+AWS+Cognito)

- cognito: uoa-pool > general settings > app clients 
    - app client name: `budget-spa`
    - untick the `Generate client secret`
    - click button `create app client`
    -> app client id: `3jfnn62bokbki2es358q9rm1c`

- cognito: uoa-pool > app integration > resource servers
    - new domain name: `uoa-budget-domain`
    - identifier: `https://budget.research.dev.auckland.ac.nz`
    - scope name: `budget-spa`, description:  `Access to budget-spa`

- cognito: uoa-pool > app integration > app clients settings
    - App client: budget-spa
    - Identity providers: `UoATestIDP`
    - callback url: `http://localhost:8100, https://budget.research.dev.auckland.ac.nz, https://oauth.pstmn.io/v1/callback, https://www.getpostman.com/oauth2/callback`
    - sign out url: `http://localhost:8100, https://budget.research.dev.auckland.ac.nz`
    - OAuth flows: `Authorization code grant` 
    - OAuth scope: `openid`, `profile`
    - Custom scope: `https://budget.research.dev.auckland.ac.nz/budget-spa`, `https://test-domain.auckland.ac.nz/lambda-hello-world`
    - click button `save changes` 
    old - callback url: `http://localhost:8100` or `https://uoa-research-nonprod.s3-website-ap-southeast-2.amazonaws.com`
    old - sign out url: `http://localhost:8100` or `https://uoa-research-nonprod.s3-website-ap-southeast-2.amazonaws.com`

- system manager > parameter store
    - name: `/dev/cognito/3jfnn62bokbki2es358q9rm1c`
    - value: `NO_AUTHORISATION_NEEDED`
    - description: `access to budget.research.dev.auckland.ac.nz`
    
### C. S3 (sandbox)
- s3: create bucket
     - bucket name: `uoa-budget-sandbox`
     - permission: untick `Block all public access`
     
- s3 > uoa-budget-sandbox > properties > static website hosting
    - `use this bucket to host a website`
    - Index document: index.html
    - Error document: index.html
    - Endpoint: http://uoa-budget-sandbox.s3-website-ap-southeast-2.amazonaws.com

- S3: left menu > block public access (account settings)
    - block all public access: `Off`, save, type `confirm`
    
- s3 > uoa-budget-sandbox > permissions > Block public access (bucket settings)
    - block all public access: `Off`, save, type `confirm`
    
- s3 > uoa-budget-sandbox > permissions > access control list
    - public access > everyone: `List objects`
    
- s3 > uoa-budget-sandbox > permissions > bucket policy
```
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "AllowPublicReadAccess",
      "Effect": "Allow",
      "Principal": "*",
      "Action": [
        "s3:GetObject"
      ],
      "Resource": [
        "arn:aws:s3:::uoa-budget-sandbox/*"
      ]
    }
  ]
}
```
### Deploy (sandbox)
- `npm run sb & npm run sg & npm run sd`
- s: sandbox, t: test, b: build, g: gen credential, d: deploy
- when sg: please enter password, and multi-factor token

### Test (sandbox)
- `http://uoa-budget-sandbox.s3-website-ap-southeast-2.amazonaws.com`
- click `protected`, show `error page`

- [html](https://www.w3schools.com/html/html_styles.asp)
- [jira](https://jira.auckland.ac.nz/secure/RapidBoard.jspa?rapidView=4941&projectKey=RBS&view=planning&issueLimit=100): mark story points

### cloudfront (sandbox)
- [AWS Host S3 Static Web Content](https://wiki.auckland.ac.nz/pages/viewpage.action?spaceKey=ITCB&title=AWS+Host+S3+Static+Web+Content)

- cloudfront
    - `create distribution`
    - web, `get started`
    - origin domain name: `uoa-budget-sandbox.s3.amazonaws.com`
    - origin id: `S3-uoa-budget-sandbox`
    - restrict bucket access: `yes`
    - origin Access Identity: `Create New Identity`
    - Grant Read Permissions on Bucket, select `Yes, Update Bucket Policy`. This will change bucket policy of `s3::uoa-budget-sandbox` as follows:
```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "BlockNonSSL",
            "Effect": "Deny",
            "Principal": "*",
            "Action": "s3:*",
            "Resource": [
                "arn:aws:s3:::uoa-budget-sandbox",
                "arn:aws:s3:::uoa-budget-sandbox/*"
            ],
            "Condition": {
                "Bool": {
                    "aws:SecureTransport": "false"
                }
            }
        },
        {
            "Sid": "2",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::cloudfront:user/CloudFront Origin Access Identity E1ZA860FKNA8R"
            },
            "Action": "s3:GetObject",
            "Resource": "arn:aws:s3:::uoa-budget-sandbox/*"
        }
    ]
}
```
- or 
```
        {
            "Sid": "AllowPublicReadAccess",
            "Effect": "Allow",
            "Principal": "*",
            "Action": "s3:GetObject",
            "Resource": "arn:aws:s3:::uoa-budget-nonprod/*"
        },
```

 - default cache behaviour settings, viewer protocol policy: suggested to use default `HTTP and HTTPS`, my preference: `Redirect HTTP to HTTPS`
 - distribution settings, 
    - Price Class:  Use All Edge Locations (Best Performance)
    - default root object: index.html
    - standard logging: `on`
    - s3 bucket for logs: `uoa-security-cloudfront-access-logs.s3.amazonaws.com`, can't find, use `uoa-sandbox-terraform.s3.amazonaws.com`
    - log prefix: `uoa-sandbox/uoa-budget-sandbox.s3.amazonaws.com`
    - click `create distribution`
    
- SSL certificate:
send email to Cloud Team:
```html
Hi Cloud Team,

Can I please have a CNAME and certificate for the following CloudFront distribution in Sandbox:

Distribution ID: E3ASPWGDJBMM7R
Domain name: d3p6hd9ysex9rl.cloudfront.net

CNAME budget.research.dev.auckland.ac.nz
``` 
    
### B. Cognito (nonprod)
- [SPA Openid-Oauth2.0 authentication and authorization with AWS Cognito](https://wiki.auckland.ac.nz/display/ITCB/SPA+Openid-Oauth2.0+authentication+and+authorization+with+AWS+Cognito)

- cognito: uoa-pool > general settings > app clients 
    - app client name: `budget-spa`
    - untick the `Generate client secret`
    - click button `create app client`
    -> app client id: `1hocu65dcmnm2rvsiao5d0iolr`

- cognito: uoa-pool > app integration > resource servers
    - new domain name: `uoa-budget-domain`
    - identifier: `https://budget.research.test.auckland.ac.nz`
    - scope name: `budget-spa`, description:  `Access to budget-spa`

- cognito: uoa-pool > app integration > app clients settings
    - App client: budget-spa
    - Identity providers: `UoATestIDP`
    - callback url: `http://localhost:8100, https://budget.research.test.auckland.ac.nz, https://oauth.pstmn.io/v1/callback, https://www.getpostman.com/oauth2/callback`
    - sign out url: `http://localhost:8100, https://budget.research.test.auckland.ac.nz`
    - OAuth flows: `Authorization code grant` 
    - OAuth scope: `openid`, `profile`
    - Custom scope: `https://budget.research.test.auckland.ac.nz/budget-spa`
    - click button `save changes` 

- system manager > parameter store
    - name: `/test/cognito/1hocu65dcmnm2rvsiao5d0iolr`
    - value: `NO_AUTHORISATION_NEEDED`
    - description: `access to budget.research.test.auckland.ac.nz`
    
### C. S3 (nonprod)
- s3: create bucket
     - bucket name: `uoa-budget-nonprod`
     - permission: untick `Block all public access`
     
- s3 > uoa-budget-sandbox > properties > static website hosting
    - `use this bucket to host a website`
    - Index document: `index.html`
    - Error document: `index.html`
    - Endpoint: `http://uoa-budget-nonprod.s3-website-ap-southeast-2.amazonaws.com`

- S3: left menu > block public access (account settings)
    - block all public access: `Off`, save, type `confirm`
    
- s3 > uoa-budget-sandbox > permissions > Block public access (bucket settings)
    - block all public access: `Off`, save, type `confirm`
    
- s3 > uoa-budget-sandbox > permissions > access control list
    - public access > everyone: `List objects`
    
- s3 > uoa-budget-sandbox > permissions > bucket policy
```
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "AllowPublicReadAccess",
      "Effect": "Allow",
      "Principal": "*",
      "Action": [
        "s3:GetObject"
      ],
      "Resource": [
        "arn:aws:s3:::uoa-budget-nonprod/*"
      ]
    }
  ]
}
```

### angular (nonprod)
- environments.nonprod.ts
```
export const environment = {
  production: true,
  version: '1.0.0-VERSION_WILL_BE_REPLACED_BY_CICD',
  auth: {
    cognitoClientId: '1hocu65dcmnm2rvsiao5d0iolr',
    cognitoUserPoolId: 'ap-southeast-2_gtuqqgIIq',
    cognitoAwsRegion: 'ap-southeast-2',
    cognitoDomain: 'uoapool-nonprod',
    redirectUri: 'https://budget.research.test.auckland.ac.nz',
    logoutUri: 'https://budget.research.test.auckland.ac.nz',
    scopes: 'openid profile https://budget.research.test.auckland.ac.nz/budget-spa',
    codeChallengeMethod: 'S256',
  },
  privateUrlKeyWords: {
    // default optional to false
    whoNeedBearerToken: [{ url: 'apigw.test.amazon.auckland.ac.nz', optional: false }],
  },
};
```
- package.json
```
...
  "scripts": {
    "ng": "ng",
    "start": "ng serve --port=8100",
    "test": "ng test",
    "lint": "ng lint",
    "e2e": "ng e2e",
    "sb": "ng build --configuration=sandbox --prod",
    "sg": "python C:\\opt\\uoa_project\\utility-aws-cli-access\\aws_saml_login.py --idp iam.auckland.ac.nz --account training --profile uoa-sandbox",
    "sd": "aws s3 sync www/ s3://uoa-budget-sandbox --delete --profile uoa-sandbox",
    "sr": "aws cloudfront create-invalidation --distribution-id E3ASPWGDJBMM7R --paths /* --profile uoa-sandbox",
    "tb": "ng build --configuration=nonprod --prod",
    "tg": "python C:\\opt\\uoa_project\\utility-aws-cli-access\\aws_saml_login.py --idp iam.auckland.ac.nz --account devops --profile uoa-nonprod",
    "td": "aws s3 sync www/ s3://uoa-budget-nonprod --delete --profile uoa-nonprod",
    "tr": "aws cloudfront create-invalidation --distribution-id E3BW0TLW1Z39TK --paths /* --profile uoa-nonprod"
  },
...
```
### Deploy (nonprod)
- `npm run tb & npm run tg & npm run td`
- s: sandbox, t: test, b: build, g: gen credential, d: deploy
- when tg: please enter password, and multi-factor token

### Test (nonprod)
- `http://uoa-budget-nonprod.s3-website-ap-southeast-2.amazonaws.com`
- click `protected`, show `error page`

### cloudfront (nonprod)
- [AWS Host S3 Static Web Content](https://wiki.auckland.ac.nz/pages/viewpage.action?spaceKey=ITCB&title=AWS+Host+S3+Static+Web+Content)

- cloudfront
    - `create distribution`
    - web, `get started`
    - origin domain name: `uoa-budget-nonprod.s3.amazonaws.com`
    - origin id: `S3-uoa-budget-nonprod`
    - restrict bucket access: `yes`
    - origin Access Identity: `Create New Identity`
    - Grant Read Permissions on Bucket, select `Yes, Update Bucket Policy`. 
    AUTOMATICALLY, This will change bucket policy of `s3::uoa-budget-nonprod` as follows:
```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "AllowPublicReadAccess",
            "Effect": "Allow",
            "Principal": "*",
            "Action": "s3:GetObject",
            "Resource": "arn:aws:s3:::uoa-budget-nonprod/*"
        },
        {
            "Sid": "2",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::cloudfront:user/CloudFront Origin Access Identity E2MI9BJYEJUQTJ"
            },
            "Action": "s3:GetObject",
            "Resource": "arn:aws:s3:::uoa-budget-nonprod/*"
        }
    ]
}
```

 - default cache behaviour settings > viewer protocol policy: suggested to use default `HTTP and HTTPS`
 - distribution settings >  
    - Price Class:  Use All Edge Locations (Best Performance)
    - default root object: index.html
    - standard logging: `on`
    - s3 bucket for logs: `uoa-security-cloudfront-access-logs.s3.amazonaws.com`, can't find, use `uoa-its-nonprod-terraform.s3.amazonaws.com`
    - log prefix: `uoa-nonprod/uoa-budget-nonprod.s3.amazonaws.com`
    - click `create distribution`
    
- goto Distributions, new cloudfront distribution created: 
    - distribution id: `E3BW0TLW1Z39TK`
    - domain name: `d2pah3cotkycir.cloudfront.net`
    
- SSL certificate:
send email to Cloud Team:

To: `ccoe@list.auckland.ac.nz`
Cc: `Nicolas Dreno <nicolas.dreno@auckland.ac.nz>; Andy Abel <andy.abel@auckland.ac.nz>; Avishek Kumar <avishek.kumar@auckland.ac.nz>; Shahnoor Islam <s.Islam@auckland.ac.nz>`
```html
Hi Cloud Team,

Can I please have a CNAME and certificate for the following CloudFront distribution:

Environment: uoa-its-nonprod
Distribution ID: E3BW0TLW1Z39TK
Domain name: d2pah3cotkycir.cloudfront.net

CNAME budget.research.test.auckland.ac.nz
``` 

   
### cloudfront
 - [Creating Amazon CloudFront Distribution](https://youtu.be/sQNONcj0cvc?t=910)
 
### route53
 - [Deploy static website to AWS with HTTPS - S3, Route 53, CloudFront, Certificate Manager](https://www.youtube.com/watch?v=lB4DTqMEumY)

#### [s3](https://youtu.be/lB4DTqMEumY?t=368)

- [result](https://youtu.be/lB4DTqMEumY?t=561)
- result: http://dadasda.asdas.dasd.asda.sdas.das 
- http is not secure, url is not friendly

- want something: https://www.abc.com
- https is secure, url is friendly

#### [route53](https://youtu.be/lB4DTqMEumY?t=602)
- dns management > create hosted zone
- *sandbox.amazon.auckland.ac.nz*, NS, route to: ns-55.awsdns-06.com.
                                       ns-1902.awsdns-45.co.uk.
                                       ns-850.awsdns-42.net.
                                       ns-1042.awsdns-02.org.

#### [certificate manager](https://youtu.be/lB4DTqMEumY?t=813)
- SSL for HTTPS user information is encrypted and transported to server
- not used: provision certificate > create certificate
- domain name: dev.auckland.ac.nz, test.auckland.ac.nz

- find an existing certificate
- sandbox: sandbox.amazon.auckland.ac.nz, maps.dev.auckland.ac.nz
- nonprod: maps.test.auckland.ac.nz, password.dev.auckland.ac.nz

1. sandbox.amazon.auckland.ac.nz
_de3ab1db3a505fde3f161e6ef1c120a4.sandbox.amazon.auckland.ac.nz.
_7b11e1081372a1c605df41c5c773f1f7.nhqijqilxf.acm-validations.aws.

2. *.sandbox.amazon.auckland.ac.nz
_de3ab1db3a505fde3f161e6ef1c120a4.sandbox.amazon.auckland.ac.nz.
_7b11e1081372a1c605df41c5c773f1f7.nhqijqilxf.acm-validations.aws.


## invalid_scope

error: `https://client_redirect_uri?error=invalid_scope`
[reference](https://docs.aws.amazon.com/cognito/latest/developerguide/authorization-endpoint.html)

- url:
    - wrong: `client_id=1hocu65dcmnm2rvsiao5d0iolr&response_type=code&redirect_uri=https://budget.research.test.auckland.ac.nz&code_challenge=-3oVDHBuDa4Bec5FNazjEqon4z9wJ67cFvzfuBJTsfI&code_challenge_method=S256&scope=openid%C2%A0profile%C2%A0https://budget.research.test.auckland.ac.nz/budget-spa`
    - correct: `client_id=1hocu65dcmnm2rvsiao5d0iolr&response_type=code&redirect_uri=http://localhost:8100&code_challenge=5mzc5sXWJ0gpmXg84-_wxj5MyCtLt_fCxyudp7UAFus&code_challenge_method=S256&scope=openid%20profile%20https://budget.research.test.auckland.ac.nz/budget-spa`

- scope: 
    - wrong: `scope=openid%C2%A0profile%C2%A0https://budget.research.test.auckland.ac.nz/budget-spa`
    - correct: `scope=openid%20profile%20https://budget.research.test.auckland.ac.nz/budget-spa`

