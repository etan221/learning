
## Ionic framework

- [Angular doc](https://angular.io/docs)
- [youtube](https://www.youtube.com/watch?v=JcEGTektejA&list=PLYxzS__5yYQng-XnJhB21Jc7NW1OIaqct)
- [ionic document](https://ionicframework.com/docs)
- [ionic components](https://ionicframework.com/docs/components)
- [ionic doc on github](https://github.com/ionic-team/ionic-docs)



### Mohsin Baig - Ionic

- [applytostudy.dev](https://applytostudy.dev.auckland.ac.nz/agency/applications/agency)
[Yesterday 3:37 PM] Mohsin Baig
```
Taylor's program id:  FSTCT
Agent id: 621426204 password: sdjtest12 . This is in DEV.
applicant: 783196469
Once you login  to the agent portal, you should be able to see the applicant application in the first 2 tabs..and it is all set up till review and submit page.
Currently running PAR process for the changes to application term..will let you know once it finishes so you can submit an
```
- [3 projects below](https://stash.auckland.ac.nz/projects/ITSSDJ/repos/sdj-fe/browse/projects)
- [ats-fe](https://stash.auckland.ac.nz/projects/ITSSDJ/repos/sdj-fe/browse/projects/ats-fe/src/app/pages/agency/shared/components)
https://stash.auckland.ac.nz/projects/ITSSDJ/repos/sdj-fe/browse

https://www.npmjs.com/search?q=uoa

- If you want to make whole app protected then your routes will be as follow:
- https://www.npmjs.com/package/@uoa/auth

- prettier format
- vscode > File > Preference > Settings, search `save`, is `format on save` clicked?

#### https://ionicframework.com/docs

- `ionic start`
- `? Project name: ionic-angular-course`
- `? Framework: Angular`
- `? Starter template: blank`
- `cd ionic-angular-course`
- `ionic serve --port=8100`

### [Core Building Blocks](https://www.udemy.com/course/ionic-2-the-practical-guide-to-building-ios-android-apps/learn/lecture/13726384#overview)
 1. *UI Components*
    - `<ion-card>`, `<ion-image>`
 2. *State Management*
    - Passing Data Around
 3. *Themes & Styles*
    - CSS & CSS Variable
 4. Ionic CLI & Publishing
    - From Development to Deployment
 - Navigation
    - Custom or Built-in Router
 - Native Device Feature: by Capacitor
    - Camera & More
 
### [Under the hood of Ionic component](https://www.udemy.com/course/ionic-2-the-practical-guide-to-building-ios-android-apps/learn/lecture/13726386#search)
- e.g. `<ion-button fill="outline" color="primary">`
    - use like a normal HTML element
    - supports attributes & properties, goto doc
    - emits events

- `<ion-button>`
    - wrap-up piece of html
    - css
    - javascript: e.g. `onClick(event)`
    - use shadow DOM + CSS Variable
    - automatically load polyfills to support older browser
    
- github source tsx file: raw source of a ionic component

### [How to learn](https://www.udemy.com/course/ionic-2-the-practical-guide-to-building-ios-android-apps/learn/lecture/13726394#search)
- e.g. [ion-button](https://ionicframework.com/docs/api/button)
 - look and feel 
 - properties
 - sample code
 - css properties

### [Core Component Types](https://www.udemy.com/course/ionic-2-the-practical-guide-to-building-ios-android-apps/learn/lecture/13726404#search)
- output: display something on the screen
    - `<ion-img>`, `<ion-badge>`, `<ion-loading>`, `<ion-label>`, `<ion-title>`, `<ion-thumbnail>`, `<ion-toolbar>`, `<ion-alert>`, `<ion-toast>`, `<ion-modal>`
- layout: to structure content, for grouping things
    - `<ion-grid>`, `<ion-row>`, `<ion-col>`, `<ion-list>`, `<ion-card>`, `<ion-infinite-scroll>`, `<ion-tabs>`
- input
    - `<ion-button>`, `<ion-input>`, `<ion-textarea>`, `<ion-menu>`, `<ion-select>`, `<ion-datetime>`, `<ion-fab>`, `<ion-toggle>`

[ion-grid](https://ionicframework.com/docs/api/grid) has rich features
### [Adding icons and using slots](https://www.udemy.com/course/ionic-2-the-practical-guide-to-building-ios-android-apps/learn/lecture/13726408#search)    

- [Ionic Icons](https://ionicons.com/)
- Slot, allow component to reserve certain spaces in their markup

Styling and Theming

### [Why Angular?](https://www.udemy.com/course/ionic-2-the-practical-guide-to-building-ios-android-apps/learn/lecture/13727688#questions)
- complex logic
    - re-invent the wheel, error-prone
    -> Angular clearly defined rules and building blocks
- state management
    - ui behaves unpredictabliy
    -> Angular provide services, bindgings and routing transport state
- routing / different pages
    - smart routing is hard
    -> Angular pass data, lazy loading etc
    
### [Angular + Ionic](https://www.udemy.com/course/ionic-2-the-practical-guide-to-building-ios-android-apps/learn/lecture/13727684#questions)
- why need angular
- setting up angular + ionic
- routing + navigation
- ionic controllrer with dependency injection

### [Setup](https://www.udemy.com/course/ionic-2-the-practical-guide-to-building-ios-android-apps/learn/lecture/13727692#questions)
- `npm install -g ionic`
- deprecated: `ionic start`
- `ionic start --help`
- `ionic start ionic-angular-course blank`
```
       - Go to your newly created project: cd .\ionic-angular-course
       - Run ionic serve within the app directory to see your app
       - Build features and components: https://ion.link/scaffolding-docs
       - Run your app on a hardware or virtual device: https://ion.link/running-docs
```

### [CSS Utility Attributes](https://www.udemy.com/course/ionic-2-the-practical-guide-to-building-ios-android-apps/learn/lecture/13727700#questions)
- css variable: 
    - store units/sizes/margins
    - in `theme/variables.scss`
    - e.g.`--ion-color-primary: #3880ff`
- can assign css variables in app to reuse these properties
- [global.scss](https://ionicframework.com/docs/layout/global-stylesheets) --> see theming and styling module
 - Refer to [Ionic Packages](https://ionicframework.com/docs/installation/cdn) for how to include the global stylesheets based on the framework and [CSS Utilities](https://ionicframework.com/docs/layout/css-utilities) for how to use the optional utilities.

- how angular connect to ionic?
 - in `angular.json`, allow ionicons
```
{
  "glob": "**/*.svg",
  "input": "node_modules/ionicons/dist/ionicons/svg",
  "output": "./svg"
}
```
 - in `ionic.config.json`, `type="angular"` lets the Ionic CLI choose the right build commands
 - in `package.json`, there is `@angular/*`, `@ionic` and most important `@ionic/angular` 
 - in `app.module.ts`, there is `import { IonicModule, IonicRouteStrategy } from '@ionic/angular';`
 - `imports: [..., IonicModule.forRoot(), ...]`-> import the ionic components
 
### [How Angular & Ionic Work Together](https://www.udemy.com/course/ionic-2-the-practical-guide-to-building-ios-android-apps/learn/lecture/13727702#questions)
- Ionic component suite can be used anywhere.
    - github: [core components](https://github.com/ionic-team/ionic-framework/tree/master/core/src/components)
- `@ionic/angular` is a wrapper. It makes usage in Angular easier and more efficient. Esp, alert-controller, modal-controller
    - [ionic-module.ts](https://github.com/ionic-team/ionic-framework/blob/master/angular/src/ionic-module.ts) - binding ionic to angular 


Managing role: Beth create budget,
 tools ad large group maintain by grouper

Joe Standon 

 project-level: jira board administrator
 role, admin panel to manage role
 - out of app, 
 - technical part how that would work, secure by api key, user permission, apikey=people, lambda, peoplesoft api, not far, 
 - sam egar, juston, 
 - on perm, account apikey, this lambda, apikey gateway, authenticate, allow to consume api, 
 - security, udi, token override, by api gateway, 
 - kafka, direct connection to peoplesoft, 
 - kong, api gateway, ACL on top of our on prem, endpoint expose,  auckland.ac.nz, kong, who is calling what? 
 - KCL, store, security or privacy, 
  - encryption, every dynamodb, budget, 
 
 - Josh Scott, not legal and hr, peoplesoft,  
 - security over transmission
 - security over storage