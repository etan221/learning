## My study notes
1. [Bitbucket/Git](01-bitbucket-git.md)
1. [AWS Lambda](02-lambda.md)
1. [AWS CLI / Serverless](03-awscli-sls.md)
1. [CRUD](04-restapi.md): API gateway / Lambda / DynamoDB
1. [setup Angular](05-angular-setup.md)
1. [Angular](06-angular.md)
