- [Set up SSH key on Bitbucket](#set-up-ssh-key-on-bitbucket)
- [[Generate SSH Key](https://confluence.atlassian.com/bitbucket/set-up-ssh-for-git-728138079.html)](#-generate-ssh-key--https---confluenceatlassiancom-bitbucket-set-up-ssh-for-git-728138079html-)
- [[Add SSH public key on Bitbucket](https://confluence.atlassian.com/bitbucket/set-up-ssh-for-git-728138079.html)](#-add-ssh-public-key-on-bitbucket--https---confluenceatlassiancom-bitbucket-set-up-ssh-for-git-728138079html-)
- [Verfiy Bitbucket login](#verfiy-bitbucket-login)
- [Git commit](#git-commit)
- [Git tag](#git-tag)
- [New Branch](#new-branch)
- [Merge branch](#merge-branch)
- [Fork and Pull Request](#fork-and-pull-request)

<small><i><a href='http://ecotrust-canada.github.io/markdown-toc/'>Table of contents generated with markdown-toc</a></i></small>

### Set up SSH key on Bitbucket
- [BitBucket Cloud](https://wiki.auckland.ac.nz/display/APPLCTN/Bitbucket+Cloud)


### [Generate SSH Key](https://confluence.atlassian.com/bitbucket/set-up-ssh-for-git-728138079.html)
Commit to bitbucket
> ssh-keygen
```
Generating public/private rsa key pair.
Enter file in which to save the key (/home/erictan/.ssh/id_rsa): 
/home/erictan/.ssh/id_rsa already exists.
Overwrite (y/n)? y
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /home/erictan/.ssh/id_rsa.
Your public key has been saved in /home/erictan/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:NQGJA2ltEleshc7muxULxvos5yITS/un1c3sgbkM55M erictan@code
The key's randomart image is:
+---[RSA 2048]----+
|    o=.=oo.      |
|    +.* +  .     |
|   . = +  o      |
|     .=  . .     |
|     o+ S        |
|  o  o.o O       |
| . +. o.B.=      |
|  = o++*Eo .     |
|   +oB=.+..      |
+----[SHA256]-----+
```

> ll ~/.ssh
```
total 8.0K
-rw------- 1 erictan erictan 1.8K Jul 27 15:18 id_rsa
-rw-r--r-- 1 erictan erictan  394 Jul 27 15:18 id_rsa.pub
```
> ssh-agent
```
SSH_AUTH_SOCK=/tmp/ssh-qC8IUN6PDBHb/agent.5088; export SSH_AUTH_SOCK;
SSH_AGENT_PID=5089; export SSH_AGENT_PID;
echo Agent pid 5089;
```
> ssh-add ~/.ssh/id_rsa
```
Enter passphrase for /home/erictan/.ssh/id_rsa: 
Identity added: /home/erictan/.ssh/id_rsa (/home/erictan/.ssh/id_rsa)
```

### [Add SSH public key on Bitbucket](https://confluence.atlassian.com/bitbucket/set-up-ssh-for-git-728138079.html)
- see *Step 4. Add the public key to your Account settings*

> cat ~/.ssh/id_rsa.pub 
```
ssh-rsa asdjkajdlasdjadadqweedascdfasdf...erictan@code
```
### Verfiy Bitbucket login
- Return to the terminal window and verify your configuration and username by entering the following command:
> ssh -T git@bitbucket.org
```
The authenticity of host 'bitbucket.org (18.205.93.2)' can't be established.
RSA key fingerprint is SHA256:zzXQOXSRBEiUtuE8AikJYKwbHaxvSc0ojez9YXaGp1A.
Are you sure you want to continue connecting (yes/no)? y
Please type 'yes' or 'no': yes
Warning: Permanently added 'bitbucket.org,18.205.93.2' (RSA) to the list of known hosts.
logged in as etan221

You can use git or hg to connect to Bitbucket. Shell access is disabled
```


> rm ~/.gitconfig
>
> git config --global user.email "eric.tan@auckland.ac.nz"

> git config --global user.name "etan221"

> git clone git@bitbucket.org:uoa/utility-aws-cli-access.git

> cd utility-aws-cli-access/

### Git commit
- `git add .` or `git add requirements.txt` 
- `git status`
```
modified:   requirements.txt
``` 
- `git commit -m 'init` or `git commit -m 'use MarkupSafe 1.1.1. Fix problem of 1.0: import name \'Feature\' from \'setuptools\''`
```
[master 8503fe0] use MarkupSafe 1.1.1. Fix problem of 1.0: import name 'Feature' from 'setuptools'
   1 file changed, 0 insertions(+), 0 deletions(-)
```
- `git status`
```
your branch is ahead of 'origin/master' by 1 commit.
```
- `git push -u origin master`

### Git tag
- [Git basic tag](https://git-scm.com/book/en/v2/Git-Basics-Tagging)
- [Git Tag](https://www.drupixels.com/blog/git-tags-guide-create-delete-push-tags-remote-and-much-more)
- `git tag -a v0.1 -m "sprint 0: login logout ionic-shell-application"`
- `git push origin --tags`

### New Branch
- `git branch -a`  should be pointing to `master`
- create new branch and switch to the new branch: `git checkout -b newfeature` or just switch to the new branch if exists already: `git checkout newfeature`
- `git branch -a`  should be pointing to `master`

### Merge branch
- [Git Merge Chinese](https://backlog.com/git-tutorial/tw/stepup/stepup2_4.html)
- in a feature branch: `git branch -a` > `sprint4-flexbox`
- switch back to master: `git checkout master`
- merge: `git merge sprint4-flexbox`
- push: `git push -u origin master`

### Fork and Pull Request
- video: [what is pull request](https://www.youtube.com/watch?v=For9VtrQx58), how to fork [on github](https://www.youtube.com/watch?v=e3bjQX9jIBk), [on bitbucket](https://www.youtube.com/watch?v=_D8kr9HZNDM)
1. [Fork a repository](https://support.atlassian.com/bitbucket-cloud/docs/fork-a-repository/)
2. modify
3. commit
4. make pull request e.g. [ionic-shell-application](https://bitbucket.org/uoa/ionic-shell-application/pull-requests/3/fixed-warning-module-not-found-error-cant)

