## Ionic Shell Application
- [ionic shell - readme](https://bitbucket.org/uoa/ionic-shell-application/src/master/README.md)
- install vscode plugin: [prettier](https://www.codereadability.com/automated-code-formatting-with-prettier/)
- `ng serve --port=8100` -> `Cannot find module '@angular-devkit/build-angular/package.json'`
- `npm install --save-dev @angular-devkit/build-angular`
- `Cannot find module 'typescript'`
- `npm install --save-dev typescript`
- `Cannot find module '@angular/compiler-cli'`
- `npm install --save-dev @angular/compiler-cli`
- `Cannot find module '@angular/compiler'`
- `npm install --save-dev @angular/compiler`
- `You seem to not be depending on "@angular/core" and/or "rxjs". This is an error.`
- `npm link`
- [Understanding Angular Routing in Ionic 4 Apps](https://www.youtube.com/watch?v=9v_yVGb41qU)
- ion-router-outlet: [video](https://www.youtube.com/watch?v=XZHG1e_prmQ), [docs](https://ionicframework.com/docs/api/router-outlet)

### flow of http request

AWS System Manager Parameter Store
- `NO_AUTHORISATION_NEEDED`
- `ManagedPrintUser.its|Employee.psrwi|Contractor.psrwi|staffIntranetUser.ec`

click protected

- `GET https://uoapool-sandbox.auth.ap-southeast-2.amazoncognito.com/oauth2/authorize?client_id=4kka8knb28m6dfvf564mqrt1vg&response_type=code&redirect_uri=http://localhost:8100&code_challenge=eOz27CFcZZeQ0s3756RAf8IrpJ4hSiHTniw9X5OmHr8&code_challenge_method=S256&scope=openid%20profile%20https://test-domain.auckland.ac.nz/lambda-hello-world`
- `302 location: https://iam.test.auckland.ac.nz/profile/SAML2/Redirect/SSO?SAMLRequest=fVJNbxshFLz3V6y4w2Lq9QfybmQljRTJVSQ76aGX6Jl9dkh3gfLYKMmvL7Zjqb7kBrx5M6MZFldv%0AfVe8YiTrXc1GQrICnfGtdfuaPT7c8hm7ar4tCPpOBb0c0rNb498BKRVLIowp7117R0OPcYPx1Rp8%0AXK9q9pxSIF2Wg4fgfccJXLv1bwIyg4DAyecDAiWuBPTw4bPo3tnkhfF9eZQrbRsiUsjsyIqbLGkd%0ApKPPM7uFXqQ8yLTmT5clBBjhPsoQ%2Fc52WG6WP1eqXGNrI5pUbjb3rLi7qdnTrN1V062Z8tFItXxc%0AyS2fGwRejSdKVmoL1fx7hhINeOcogUs1U1JJLmdcTR%2FkXMuJHs2FnKjfrPh1zk8d8suJOtKnxGo2%0ARKc9kCXtoEfSyeiDK52hOttM3viONaeA9VEwFrc%2B9pC%2B3j282JbvjlCNLtn0fqH99Tqcy2PNAXaq%0AQH92oCnoy46ewv5HfHlfje8X5f9Wm8%2Fr5ddo%2FgE%3D&RelayState=ZXlKMWMyVnlVRzl2YkVsa0lqb2lZWEF0YzI5MWRHaGxZWE4wTFRKZmNHZEZjbXA1VERSUElpd2ljSEp2ZG1sa1pYSk9ZVzFsSWpvaVZXOUJWR1Z6ZEVsRVVDSXNJbU5zYVdWdWRFbGtJam9pTkd0cllUaHJibUl5T0cwMlpHWjJaalUyTkcxeGNuUXhkbWNpTENKeVpXUnBjbVZqZEZWU1NTSTZJbWgwZEhBNkx5OXNiMk5oYkdodmMzUTZPREV3TUNJc0luSmxjM0J2Ym5ObFZIbHdaU0k2SW1OdlpHVWlMQ0p3Y205MmFXUmxjbFI1Y0dVaU9pSlRRVTFNSWl3aWMyTnZjR1Z6SWpwYkltOXdaVzVwWkNJc0luQnliMlpwYkdVaUxDSm9kSFJ3Y3pvdkwzUmxjM1F0Wkc5dFlXbHVMbUYxWTJ0c1lXNWtMbUZqTG01NkwyeGhiV0prWVMxb1pXeHNieTEzYjNKc1pDSmRMQ0p6ZEdGMFpTSTZiblZzYkN3aVkyOWtaVU5vWVd4c1pXNW5aU0k2SW1WUGVqSTNRMFpqV2xwbFVUQnpNemMxTmxKQlpqaEpjbkJLTkdoVGFVaFVibWwzT1ZnMVQyMUljamdpTENKamIyUmxRMmhoYkd4bGJtZGxUV1YwYUc5a0lqb2lVekkxTmlJc0ltNXZibU5sSWpvaWFuTlRabUZaY25CSFMxcEhTbFZaUVZGSlNtWk1kVkl3TW5kaWJHZDBVbEJJTFVoWVJsOXpVRGgyUWxSZlZFVjJNQzFOUmtoUlVXSnlPVkV6TldVMmJucE1iM04wVTNRMk1HcExRV2t5VVhOcGExWmZiVGgyYldOU09UZHFRazloZUhScFozaFpjRlZUYzBkWFZYVk9MVWMwVkY5Wk4zWnlYMkpmUWpKRE1HRk1NSGROWkZaT1ZWcE1OWFJ6Y0VJemVHVk1VbWs1TWkxWFJVWnhlREEzUkZkNk0zaFFVWEJSZUdSUklpd2ljMlZ5ZG1WeVNHOXpkRkJ2Y25RaU9pSjFiMkZ3YjI5c0xYTmhibVJpYjNndVlYVjBhQzVoY0MxemIzVjBhR1ZoYzNRdE1pNWhiV0Y2YjI1amIyZHVhWFJ2TG1OdmJTSXNJbU55WldGMGFXOXVWR2x0WlZObFkyOXVaSE1pT2pFMU9UZzFNVGt4Tnprc0luTmxjM05wYjI0aU9tNTFiR3dzSW5WelpYSkJkSFJ5YVdKMWRHVnpJanB1ZFd4c0xDSnBjMU4wWVhSbFJtOXlUR2x1YTJsdVoxTmxjM05wYjI0aU9tWmhiSE5sZlE9PTpHWmlEQXN5N0lXN2hvbW9zSzZxaGd1NGo1N09Yb1c2M3JKV0IrSUJkcmF3PToy`

- `GET https://iam.test.auckland.ac.nz/profile/SAML2/Redirect/SSO?SAMLRequest=fVJNbxshFLz3V6y4w2Lq9QfybmQljRTJVSQ76aGX6Jl9dkh3gfLYKMmvL7Zjqb7kBrx5M6MZFldv%0AfVe8YiTrXc1GQrICnfGtdfuaPT7c8hm7ar4tCPpOBb0c0rNb498BKRVLIowp7117R0OPcYPx1Rp8%0AXK9q9pxSIF2Wg4fgfccJXLv1bwIyg4DAyecDAiWuBPTw4bPo3tnkhfF9eZQrbRsiUsjsyIqbLGkd%0ApKPPM7uFXqQ8yLTmT5clBBjhPsoQ%2Fc52WG6WP1eqXGNrI5pUbjb3rLi7qdnTrN1V062Z8tFItXxc%0AyS2fGwRejSdKVmoL1fx7hhINeOcogUs1U1JJLmdcTR%2FkXMuJHs2FnKjfrPh1zk8d8suJOtKnxGo2%0ARKc9kCXtoEfSyeiDK52hOttM3viONaeA9VEwFrc%2B9pC%2B3j282JbvjlCNLtn0fqH99Tqcy2PNAXaq%0AQH92oCnoy46ewv5HfHlfje8X5f9Wm8%2Fr5ddo%2FgE%3D&RelayState=ZXlKMWMyVnlVRzl2YkVsa0lqb2lZWEF0YzI5MWRHaGxZWE4wTFRKZmNHZEZjbXA1VERSUElpd2ljSEp2ZG1sa1pYSk9ZVzFsSWpvaVZXOUJWR1Z6ZEVsRVVDSXNJbU5zYVdWdWRFbGtJam9pTkd0cllUaHJibUl5T0cwMlpHWjJaalUyTkcxeGNuUXhkbWNpTENKeVpXUnBjbVZqZEZWU1NTSTZJbWgwZEhBNkx5OXNiMk5oYkdodmMzUTZPREV3TUNJc0luSmxjM0J2Ym5ObFZIbHdaU0k2SW1OdlpHVWlMQ0p3Y205MmFXUmxjbFI1Y0dVaU9pSlRRVTFNSWl3aWMyTnZjR1Z6SWpwYkltOXdaVzVwWkNJc0luQnliMlpwYkdVaUxDSm9kSFJ3Y3pvdkwzUmxjM1F0Wkc5dFlXbHVMbUYxWTJ0c1lXNWtMbUZqTG01NkwyeGhiV0prWVMxb1pXeHNieTEzYjNKc1pDSmRMQ0p6ZEdGMFpTSTZiblZzYkN3aVkyOWtaVU5vWVd4c1pXNW5aU0k2SW1WUGVqSTNRMFpqV2xwbFVUQnpNemMxTmxKQlpqaEpjbkJLTkdoVGFVaFVibWwzT1ZnMVQyMUljamdpTENKamIyUmxRMmhoYkd4bGJtZGxUV1YwYUc5a0lqb2lVekkxTmlJc0ltNXZibU5sSWpvaWFuTlRabUZaY25CSFMxcEhTbFZaUVZGSlNtWk1kVkl3TW5kaWJHZDBVbEJJTFVoWVJsOXpVRGgyUWxSZlZFVjJNQzFOUmtoUlVXSnlPVkV6TldVMmJucE1iM04wVTNRMk1HcExRV2t5VVhOcGExWmZiVGgyYldOU09UZHFRazloZUhScFozaFpjRlZUYzBkWFZYVk9MVWMwVkY5Wk4zWnlYMkpmUWpKRE1HRk1NSGROWkZaT1ZWcE1OWFJ6Y0VJemVHVk1VbWs1TWkxWFJVWnhlREEzUkZkNk0zaFFVWEJSZUdSUklpd2ljMlZ5ZG1WeVNHOXpkRkJ2Y25RaU9pSjFiMkZ3YjI5c0xYTmhibVJpYjNndVlYVjBhQzVoY0MxemIzVjBhR1ZoYzNRdE1pNWhiV0Y2YjI1amIyZHVhWFJ2TG1OdmJTSXNJbU55WldGMGFXOXVWR2x0WlZObFkyOXVaSE1pT2pFMU9UZzFNVGt4Tnprc0luTmxjM05wYjI0aU9tNTFiR3dzSW5WelpYSkJkSFJ5YVdKMWRHVnpJanB1ZFd4c0xDSnBjMU4wWVhSbFJtOXlUR2x1YTJsdVoxTmxjM05wYjI0aU9tWmhiSE5sZlE9PTpHWmlEQXN5N0lXN2hvbW9zSzZxaGd1NGo1N09Yb1c2M3JKV0IrSUJkcmF3PToy`
- `200 OK`

- `POST https://uoapool-sandbox.auth.ap-southeast-2.amazoncognito.com/saml2/idpresponse`
- `302 location: http://localhost:8100?code=80ab16c9-eba2-4bbd-8bfa-933b165d0592`

- `http://localhost:8100/?code=80ab16c9-eba2-4bbd-8bfa-933b165d0592`

- `GET https://cognito-idp.ap-southeast-2.amazonaws.com/ap-southeast-2_pgErjyL4O/.well-known/openid-configuration`
- `content-type: application/json`

- `POST https://uoapool-sandbox.auth.ap-southeast-2.amazoncognito.com/oauth2/token`
- `content-type: application/json;charset=UTF-8`

## How to Login, example: uoa-wayfinding 

- `@uoa/auth/lib/core/servies/login.service.d.ts#LoginService`
```
export declare class LoginService {
    constructor(_authService: AuthService, _storageService: StorageService, _router: Router, _cognitoConfig: CognitoConfigService);
    isAuthenticated(): Promise<boolean>;
    doLogin(targetReturnUrl?: string): Promise<boolean>;
    /**
     *  It is to handle login success case only to use returned code to get token, not Authentication.
     *  So it should always return true or redirect if there is an error from server.
     */
    loginSuccess(state: RouterStateSnapshot): Promise<boolean | UrlTree>;
    getUserInfo(): Promise<UserInfoDto>;
    logout(): void;
}
```

### isAuthenticated(): Promise<boolean>;
- `src/app/shared/components/menu/menu.component.ts#MenuComponent`
```
export class MenuCompoent implements OnInit{
    public async ngOnInit(){
        this.loggedIn = await this._loginService.isAuthenticated();
        this.user = await this._loginService.getUserInfo();
    }
}
```
- `src/app/shared/components/header/header.component.ts#HeaderComponent`
```
export class HeaderCompoent implements OnInit{
    async ngOnInit(){
        this.loggedIn = await this._loginService.isAuthenticated();
    }
}
```

### doLogin(targetReturnUrl?: string): Promise<boolean>;

- `src/app/shared/components/menu/menu.component.html#MenuComponent`
```
<ng-template [ngIf]="!loggedIn" [ngIfElse]="logedIn">
  <ion-menu-toggle color="primary" fill="clear">
    <a #login (click)="doLogin()" (keydown.enter)="doLogin()" tabindex="0">
      Log in/ Registration
    </a>
  </ion-menu-toggle>
</ng-template>
```
- `src/app/shared/components/menu/menu.component.ts/MenuComponent`
```
  public async doLogin() {
    await this._loginService.doLogin(this._router.url);
  }

  public async recentCheckIn(): Promise<void> {
    const authenticate = await this._loginService.doLogin(this._router.url);
  }

  public async checkOut(): Promise<void> {
    const authenticate = await this._loginService.doLogin(this._router.url);
  }
```
- `src/app/shared/components/indoor-map/location-info-card/location-info-card.component.ts/LOcationInfoCardComponent`

```
  public async doCheckIn() {
    const isAuthenticated = await this._loginService.doLogin(this._router.url);
    ...
  }

  public async recentCheckIn(): Promise<void> {
    const isAuthenticated = await this._loginService.doLogin(this._router.url);
    ...
  }

```
### loginSuccess(state: RouterStateSnapshot): Promise<boolean | UrlTree>;
- none

### getUserInfo(): Promise<UserInfoDto>;

- src/app/core/services/event.service.ts#EventService
```
  public async showCheckedInDetails(checkinSuccess: EventDto): Promise<void> {
    const userInfo = await this._loginService.getUserInfo();
    this.dismissLoading();
    const modal = await this._modalCtrl.create({
      backdropDismiss: false,
      component: CheckedinDetailModalComponent,
      componentProps: {
        checkinSuccess,
        userInfo,
      },
    });
    return modal.present();
  }
```

- src/app/shared/components/menu/menu.component.ts#MenuComponent 
```
  public async ngOnInit() {
    this.loggedIn = await this._loginService.isAuthenticated();
    this.user = await this._loginService.getUserInfo();
    if (this.loggedIn) {
      this._eventService.getEventsHistory();
    }
  }
```

### logout(): void;
- none

## routing
- `npm install`
- `npm start`

- [Learn RxJS in 30 days: zh-TW](https://ithelp.ithome.com.tw/articles/10189028)
    - [Functional Programming](https://blog.jerry-hong.com/series/rxjs/thirty-days-RxJS-02/)
    - [Array.filter, map, foreach](https://blog.jerry-hong.com/series/rxjs/thirty-days-RxJS-03/)
    - [Observer Pattern, Iterator Pattern](https://blog.jerry-hong.com/series/rxjs/thirty-days-RxJS-04/)
    - [BehaviorSubject & ReplaySubject & AsyncSubject](https://blog.jerry-hong.com/series/rxjs/thirty-days-RxJS-23/) 

- [Will](https://www.youtube.com/watch?v=BA1vSZwzkK8)
- [一次搞懂 RxJS 與 Observable](https://www.youtube.com/watch?v=wHa_0x81OZU)

-[RxJS - What and Why?](https://www.youtube.com/watch?v=T9wOu11uU6U)
 - "RxJS is a library for reactive programming using Observables, to make it easier to compose asynchronous or callback-based code.", [RxJS](http://reactivex.io/rxjs/manual/overview.html)
 
 http://reactivex.io/rxjs/manual/installation.html